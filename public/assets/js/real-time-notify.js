
$(document).ready(function(){

   // when user click on bell icon hide the blue badge showing the number of notifications and
   // send an ajax request to mark all the notifications as read
   $("#notify-bell").click(function(){
      $("#notify-count").css('display','none');
    });

	function get_notifications(){
		$.ajax({
			url:'/get_notifications',
			method:'GET',
			dataType:'json',
			success:function(data){
				// add the notifaction details to notification dropdown menu
				$("#notify-dropdown").html(data.notification_details);
                
                // if there one or more notification(s) then show the badge on  bell icon 
                // showing the number of new notifications
				if(data.notification_count > 0){
					$("#notify-count").css('display','inline-block');
					$("#notify-count").html(data.notification_count);
				}
			}
		});
	}
    
    //get_notifications();
    
    // call get_notifications method every five seconds
    setInterval(function(){
    	get_notifications();
    }, 5000);

   


});
