<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'title', 'description','assign_by', 'assign_to', 'assign_date', 'weight', 'status', 'dead_line', 'work_place'
    ];

}
