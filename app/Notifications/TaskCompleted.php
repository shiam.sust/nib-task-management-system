<?php

namespace App\Notifications;

use Auth;
use App\Task;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskCompleted extends Notification
{
    use Queueable;

    protected $assigned_by;
    protected $assigned_to;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($assigned_by)
    {
       $this->assigned_by = $assigned_by; 
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
       $this->assigned_to = Auth::user();
        $completed_task = Task::where('assign_to', $this->assigned_to->id)
                              ->where('assign_by', $this->assigned_by->id)
                              ->where('status','Pending')
                              ->where('viewd', 1)
                              ->first();
        
        $message = $this->assigned_to->name." completed a task assigned by you.";
        $image_path = $this->assigned_to->image;
        $url = "view_completed_tasks";
        $viewd = $completed_task->viewd;
        
        return [
            'image_path' => $image_path,
            'message' => $message,
            'url' => $url,
            'viewd' => $viewd
        ];
    }
}
