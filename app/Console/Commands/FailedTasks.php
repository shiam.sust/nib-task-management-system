<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;
use App\CFTask;

class FailedTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FailedTasks:find_failed_tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will find the failed tasks everyday and change their status to failed.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = date('Y-m-d'); // get todays date

        /*
        |=================================================
        | Query explanation:
        |=================================================
        | Find the ongoing and pending tasks whose deadline less than todays date.
        |
        |*/

        $failed_tasks = Task::WhereDate('dead_line','<', $today)
                             ->where(function ($query) {
                             $query->where('status', 'Pending')
                             ->orWhere('status', 'Ongoing');
                            })
                            ->get();

        /*
        * Now change the status of all the 'ongoing' and 'pending' tasks whose deadline is over
        * to 'Failed'. And for failed tasks create entry in database entry name 'c_f_tasks' 
        * through CFTasks model.
        */
        foreach($failed_tasks as $failed_task){
            $failed_task->status = 'Failed'; // updated task status
            $failed_task->save();


        /**
        * If the place of work was outside NIB then change the value of location field of 'users'
        * table to NIB now.
        *
        */

        if($failed_task->work_place != 'NIB'){
            $user = User::find($failed_task->assign_to);
            $user->location = 'NIB';
            $user->save();
        }

            
            // create new entry for new failed tasks in 'c_f_task' database table
            $_failed_task = new CFTask; 
            $_failed_task->task_id = $failed_task->id;
            $_failed_task->save();
        }
    }
}
