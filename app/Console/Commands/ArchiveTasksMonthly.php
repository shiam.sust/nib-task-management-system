<?php

namespace App\Console\Commands;

use App\Task;
use App\CFTask;
use App\ArchivedCompletedTask;
use App\ArchivedFailedTask;

use Illuminate\Console\Command;

class ArchiveTasksMonthly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'archive:tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Every month delete all the "failed" and "completed" tasks and add them to _tasks table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all the tasks which are completed
        $completed_tasks = Task::join('c_f_tasks ','tasks.id','=','c_f_tasks .task_id')
                              ->where('tasks.status','=','Completed')
                              ->get();                         

        // now insert all the completed tasks into archived_completed_tasks table and 
        // delete their entry from tasks and c_f_tasks table.
        foreach($completed_tasks as $completed_task){
            
            $archived_completed_task = new ArchivedCompletedTask;

            $archived_completed_task->task_id = $completed_task->task_id;
            $archived_completed_task->title = $completed_task->title;
            $archived_completed_task->description = $completed_task->description;
            $archived_completed_task->assign_by = $completed_task->assign_by;
            $archived_completed_task->assign_to = $completed_task->assign_to;
            $archived_completed_task->weight = $completed_task->weight;
            $archived_completed_task->assign_date = $completed_task->assign_date;
            $archived_completed_task->status = $completed_task->status;
            $archived_completed_task->dead_line = $completed_task->dead_line;
            $archived_completed_task->work_place = $completed_task->work_place;
            $archived_completed_task->mark = $completed_task->mark;
            $archived_completed_task->comment = $completed_task->comment;
            $archived_completed_task->file_path = $completed_task->file_path;
            
            $archived_completed_task->save();
            

            $delete_task = Task::find($completed_task->task_id);
            $delete_task->delete(); // delete task entry from tasks table
            
            // since task_id is not primary key in c_f_tasks table we used 
            // where clause
            $delete_task = CFTask::where('task_id','=', $completed_task->task_id)->first();
            $delete_task->delete(); // delete task entry from c_f_tasks table

        }

        // get all tasks which are failed
        $failed_tasks = Task::join('c_f_tasks ','tasks.id','=','c_f_tasks .task_id')
                              ->where('tasks.status','=','Failed')
                              ->get(); 

        // now insert all the completed tasks into archived_completed_tasks table and 
        // delete their entry from tasks and c_f_tasks table.

        foreach($failed_tasks as $failed_task){
            
            $archived_failed_task = new ArchivedFailedTask;

            $archived_failed_task->task_id = $failed_task->id;
            $archived_failed_task->title = $failed_task->title;
            $archived_failed_task->description = $failed_task->description;
            $archived_failed_task->assign_by = $failed_task->assign_by;
            $archived_failed_task->assign_to = $failed_task->assign_to;
            $archived_failed_task->weight = $failed_task->weight;
            $archived_failed_task->assign_date = $failed_task->assign_date;
            $archived_failed_task->status = $failed_task->status;
            $archived_failed_task->dead_line = $failed_task->dead_line;
            $archived_failed_task->work_place = $failed_task->work_place;
            $archived_failed_task->explain = $failed_task->explain;

            $archived_failed_task->save();
            
            $delete_task = Task::find($failed_task->task_id);
            $delete_task->delete(); // delete task entry from tasks table
            
            // since task_id is not primary key in c_f_tasks table we used 
            // where clause
            
            $delete_task = CFTask::where('task_id','=',$failed_task->task_id)->first();
            $delete_task->delete(); // delete task entry from c_f_tasks table
            
        }

        }
}
