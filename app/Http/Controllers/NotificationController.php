<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Task;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
   /**
    * Get the new notifications generated within in the interval defined in script
    *
    * @return Illuminate\Http\Response 
    *
    */
   public function get_notifications(){
   	   $user = Auth::user();
        
        $notification_count = count($user->unreadNotifications);
   	    $notification_details = '<li>
            <div class="notification_header">
                <h3>Notifications</h3>
            </div>
        </li>';
       $index = 0;
       foreach ($user->notifications as $notification){
        
        if($index == 5) 
        	break;

        // count number of notifications sent to user
        // we will show maximum 5 notifications when user click on bell icon
        // to view all notifications user must to click on the see all notifications link
        $index++; 

        // The following portion will be used to calculate the time spent after the
        // the notification was created and format that time difference somewhat 
        // as seen in  Facebook notifications 
        
        $current_time = date_create(date("Y-m-d H:i:s"));
        // date_diff(previous_time, next_time)
        $time_diff = date_diff($notification->created_at, $current_time);

        $year = (int) $time_diff->format("%y");
		$month = (int) $time_diff->format("%m");
		$day = (int) $time_diff->format("%d");
		$week = (int) ($day / 7);
		$hour = (int) $time_diff->format("%h");
		$minute = (int) $time_diff->format("%i");
		$second = (int) $time_diff->format("%s");

        $time_difference = null;

		if($year != 0){
			if($year == 1)
				if($month > 6)
					$time_difference = "about 2 years ago";
				else
					$time_difference = "a year ago";
		    
		    elseif($year > 1 )
		    	if($month > 6){
		    		$year += 1;
					$time_difference = "about " .$year." years ago";
		    	}
				else
					$time_difference = $year." years ago";
		}

		elseif($month != 0){
			if($month == 1)
				if($day > 15)
					$time_difference = "about 2 months ago";
				else
					$time_difference = "a month ago";
		    
		    elseif($month > 1 )
		    	if($day > 15){
		    		$month += 1;
					$time_difference = "about " .$month." months ago";
		    	}
				else
					$time_difference = $month." months ago";
		}

		elseif($week != 0){
			if($week == 1)
				if($day > 4)
					$time_difference = "about 2 weeks ago";
				else
					$time_difference = "a week ago";
		    
		    elseif($week > 1 )
		    	if($day > 4){
		    		$week += 1;
					$time_difference = "about " .$week." weeks ago";
		    	}
				else
					$time_difference = $week." weeks ago";
		}

		elseif($day != 0){
			if($day == 1)
				if($hour > 12)
					$time_difference = "about 2 days ago";
				else
					$time_difference = "a day ago";
		    
		    elseif($day > 1 )
		    	if($hour > 12){
		    		$day += 1;
					$time_difference = "about " .$day." days ago";
		    	}
				else
					$time_difference = $day." days ago";
		}

		elseif($hour != 0){
			if($hour == 1)
				if($minute > 30)
					$time_difference = "about 2 hours ago";
				else
					$time_difference = "an hour ago";
		    
		    elseif($hour > 1 )
		    	if($minute > 30){
		    		$hour += 1;
					$time_difference = "about " .$hour." hours ago";
		    	}
				else
					$time_difference = $hour." hours ago";
		}

		elseif($minute != 0){
			if($minute == 1)
				if($second > 30)
					$time_difference = "about 2 minutes ago";
				else
					$time_difference = "a minute ago";
		    
		    elseif($minute > 1 )
		    	if($second > 30){
		    		$minute += 1;
					$time_difference = "about " .$minute." minutes ago";
		    	}
				else
					$time_difference = $minute." minutes ago";
		}

		elseif($second != 0){
			if($second == 1)
			$time_difference = "a second ago";
		    
		    elseif($second > 1)
		    $time_difference = $second." seconds ago";
		}

       	$url = route($notification->data['url']); // get actual route to given named route
        $notification_details .= '<li>
            <a href= " ' . $url . ' ">';
        
        // if there is profile image then show it in notifications
        if($notification->data['image_path'] != null){
        	$image_url = URL::to($notification->data['image_path']);
            $notification_details .= '<div class="user_img"><img src="' . $image_url . '" alt="Assigned_By_User"></div>';
        }
        
        // otherwise show image person placeholder
        elseif($notification->data['image_path'] == null){ 
        	$image_url = asset('assets/images/img-person-placeholder.jpg');
            $notification_details .= '<div class="user_img"><img src="' . $image_url . '" alt="Assigned_By_User"></div>';
        }
       
       $notification_details .= '<div class="notification_desc">
         <p>' . $notification->data['message'] . '</p>
         <p><span>' . $time_difference . '</span></p>
        </div>
       <div class="clearfix"></div>  
      </a>
    </li>';
    }
        
        $notification_details .=  '<li>
            <div class="notification_bottom">
                <a href="#">See all notifications</a>
            </div> 
        </li>';

        $data = array('notification_count' => $notification_count,
                      'notification_details' => $notification_details);
        $data = json_encode($data);
        return $data;
   }

    /**
    * Show all pending tasks that were not viewd before to user
    *
    * @return Illuminate\Http\Response 
    *
    */
    public function view_pending_tasks(){
        
        $user = Auth::user();
    	$user_id = $user->id; 
        /*
        $tasks = Task::where([['assign_by','<>',$user_id],['assign_to','=',$user_id]])
                       ->orderBy('created_at','desc')
                       ->get();
        */

        /**
        * The following query gets the pending tasks assigned by user to himself with name of 
        * the officer  who assigned the task and the name of the officer to whom the task 
        * was assigned 
        *
        */

        $sub_query = Task::join('users', 'assign_by', '=', 'users.id')
                           ->select('tasks.id AS task_id',   'title', 'assign_by', 'assign_to', 'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'name   AS  assign_by_name', 'viewd');

        $pending_tasks = DB::table(DB::raw("({$sub_query->toSql()}) as sub_query"))
                     ->join('users', 'assign_to', '=', 'users.id')
                     ->where('assign_by','<>',$user_id)
                     ->where('assign_to','=',$user_id)
                     ->where('status','Pending')
                     ->where('viewd','0')
                     ->select('task_id',   'title', 'assign_by', 'assign_to',    'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'assign_by_name', 'name AS assign_to_name')
                     ->orderBy('created_at', 'desc')
                     ->get();

        // count the number of pending tasks assigned to Auth user that has not been viewd by 
        // him
        $pending_tasks_count = Task::where('assign_by','<>',$user_id)
			                     ->where('assign_to','=',$user_id)
			                     ->where('status','Pending')
			                     ->where('viewd','0')
			                     ->count();

    // when user click any one of pending task shown in notification details we will show 
    // all the unviewd pending tasks as list and mark all the unviewd pending tasks as viewd
    foreach($pending_tasks as $pending_task){
        $task = Task::find($pending_task->task_id); // get task Model
        $task->viewd = 1; // mark task as viewd
        $task->save();
    }
    
    // besides we will mark the notification as read when user click on any notification to see 
    // details about notification
    foreach($user->unreadNotifications as $notification)
   		$notification->markAsRead();
   
    return view('tasks.p_o_tasks.view_pending_tasks', compact('pending_tasks_count','pending_tasks'));
    }


    /**
    * Show all completed tasks that were not viewd before to user
    *
    * @return Illuminate\Http\Response 
    *
    */
    public function view_completed_tasks(){
        
        $user = Auth::user();
        $user_id = $user->id; 
        /*
        $tasks = Task::where([['assign_by','<>',$user_id],['assign_to','=',$user_id]])
                       ->orderBy('created_at','desc')
                       ->get();
        */

        /**
        * The following query gets the completed tasks assigned by Authenticated user with name of 
        * the Authenticated user and the name of the user to whom the task 
        * was assigned. 
        *
        */

        $sub_query = Task::join('users', 'assign_by', '=', 'users.id')
                         ->select('tasks.id AS task_id',   'title', 'assign_by', 'assign_to', 'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'name   AS  assign_by_name', 'viewd');

        $completed_tasks = DB::table(DB::raw("({$sub_query->toSql()}) as sub_query"))
                     ->join('users', 'assign_to', '=', 'users.id')
                     ->where('assign_by','=',$user_id)
                     ->where('assign_to','<>',$user_id)
                     ->where('status','Completed')
                     ->where('viewd','1')
                     ->select('task_id',   'title', 'assign_by', 'assign_to',    'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'assign_by_name', 'name AS assign_to_name')
                     ->orderBy('created_at', 'desc')
                     ->get();

        // count the number of completed tasks assigned by Auth user that has not been viewd by 
        // him
        $completed_tasks_count = Task::where('assign_by','=',$user_id)
                                 ->where('assign_to','<>',$user_id)
                                 ->where('status','Completed')
                                 ->where('viewd','1')
                                 ->count();

    // when user click any one of completed task shown in notification details we will show 
    // all the unviewd completed tasks as list and mark all the unviewd completed tasks as viewd
    foreach($completed_tasks as $completed_task){
        $task = Task::find($completed_task->task_id); // get task Model
        $task->viewd = 0; // mark task as viewd
        $task->save();
    }
    
    // besides we will mark the notification as read when user click on any notification to see 
    // details about notification
    foreach($user->unreadNotifications as $notification)
        $notification->markAsRead();
   
    return view('tasks.c_f_tasks.view_completed_tasks', compact('completed_tasks_count','completed_tasks'));
    }
}
