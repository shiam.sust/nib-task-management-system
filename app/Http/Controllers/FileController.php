<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Task;
use App\CFTask;
use App\Notifications\TaskCompleted;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;


class FileController extends Controller
{
    /**
     * Upload file from self_assigned_tasks page as a proof of completing task
     *
     * @param  $task_id
     * @return \Illuminate\Http\Response
     *
     */   
     public function upload_file_self($task_id) 
     {
        return view('upload_file_self', compact('task_id'));
     } 

     /**
     * Save uploaded file to drives. Then update the status of the task to 'Complete' and  * save the path to the file to database table.
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */   
     public function store_image_self(Request $request) 
     {
        
      /**
     	* User will be allowed to upload image, pdf file, word document, excel sheet only.
     	*/
        
        $this->validate($request, [
            'fileToUpload'=>'required|mimes:jpeg,png,jpg,gif,svg,xls,xlsx,pdf,doc,docx,zip|max:2048'
        ]);
        
        if($request->hasFile('fileToUpload')){
          
          $file = $request->file('fileToUpload');
          $extension = $file->getClientOriginalExtension();

          $file_name = "task_".$request->task_id.".".$extension; // create a file name such as task_1.jpg, task_2.jpg etc.
          
          //$file_path = $request->file('fileToUpload')->storeAs('UploadedImages', $file_name);
          

          /*
          | Use separate folders to store image, word document, excel and pdf file.
          | 
          |
          */ 
          
        if($extension == "doc" or $extension == "docx"){
          $dest_path = storage_path('app/public/words');
          $file_path = "storage/words/".$file_name; // path to the word files in public folder
        }

        elseif($extension == "xls" or $extension == "xlsx"){
          $dest_path = storage_path('app/public/excels');
          $file_path = "storage/excels/".$file_name; // path to the excel sheets in public folder
        }

        elseif($extension == "pdf"){
          $dest_path = storage_path('app/public/pdfs');
          $file_path = "storage/pdfs/".$file_name; // path to the pdf files in public folder
        }

        elseif($extension == "jpeg" or $extension == "png" or $extension == "jpg" or $extension == "gif" or $extension == "svg"){
          $dest_path = storage_path('app/public/images');
          $file_path = "storage/images/".$file_name; // path to the images in public folder
        }
          
          $file->move($dest_path, $file_name);

          $task = Task::find($request->task_id); // fetch current task status from database
          $task->status = 'Completed'; // update task status to 'Completed'
          $task_update = $task->save(); // save the changed status to database.

        /**
        * If the place of work was outside NIB then change the value of location field of 
        * 'users' table to NIB now since the task is now completed.
        *
        */

        if($task->work_place != 'NIB'){
            $user = User::find($task->assign_to); // get user details
            $user->location = 'NIB'; // change current location
            $user->save();
        }
        
         // print($file_path);
          /*
          * c_f_task store data related to completed and failed tasks such as 'mark,      * 'comment','explain', and 'file_path'. When a task is completed through 
          * submitting file add a entry for that task in c_f_task table.
          */
          $cftask = new CFTask;
          $cftask->task_id = $request->task_id;
          $cftask->file_path = $file_path; // store path to file to database

          $cftask_add = $cftask->save();

        /**
        * Redirect to self_assigned_tasks once again.
        *  
        */
       
        if($task_update and $cftask_add)
        return redirect()->route('self_assigned_tasks', $request->task_id)->with('success','Congratulations! Your file was uploaded successfully.');
    }
}
 

    /**
     * Upload file as a proof of completing task
     *
     *
     */   
     public function upload_file_authority($task_id) 
     {
        return view('upload_file_authority', compact('task_id'));
     } 

     /**
     * Save uploaded file to drives. Then update the status of the task to 'Complete' and save *  the path to the file to database table.
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */   
     public function store_image_authority(Request $request) 
     {

     	/**
     	* User will be allowed to upload image, pdf file, word document, excel sheet only.
     	*/
        
        $this->validate($request, [
            'fileToUpload'=>'required|mimes:jpeg,png,jpg,gif,svg,xls,xlsx,pdf,doc,docx,zip|max:2048'
        ]);
        
        if($request->hasFile('fileToUpload')){
          
          $file = $request->file('fileToUpload');
          $extension = $file->getClientOriginalExtension();

          $file_name = "task_".$request->task_id.".".$extension; // create a file name such as task_1.jpg, task_2.jpg etc.

          //$file_path = $request->file('fileToUpload')->storeAs('UploadedImages', $file_name);
          
          /*
          | Use separate folders to store image, word document, excel and pdf file.
          | 
          |
          */ 
          
        if($extension == "doc" or $extension == "docx"){
          $dest_path = storage_path('app/public/words');
          $file_path = "storage/words/".$file_name; // path to the word files in public folder
        }

        elseif($extension == "xls" or $extension == "xlsx"){
          $dest_path = storage_path('app/public/excels');
          $file_path = "storage/excels/".$file_name; // path to the excel sheets in public folder
        }

        elseif($extension == "pdf"){
          $dest_path = storage_path('app/public/pdfs');
          $file_path = "storage/pdfs/".$file_name; // path to the pdf files in public folder
        }

        elseif($extension == "jpeg" or $extension == "png" or $extension == "jpg" or $extension == "gif" or $extension == "svg"){
          $dest_path = storage_path('app/public/images');
          $file_path = "storage/images/".$file_name; // path to the images in public folder
        }

          $file->move($dest_path, $file_name);
          //$this->save();
        

          $task = Task::find($request->task_id); // fetch current task status from database
          $task->status = 'Completed'; // update task status to 'Completed'
          $task_update = $task->save(); // save the changed status to database.
        
         // print($file_path);
          /*
          * c_f_task store data related to completed and failed tasks such as 'mark,            * 'comment','explain', and 'file_path'. When a task is completed through submitting
          * file add a entry for that task in c_f_task table.
          */
          $cftask = new CFTask;
          $cftask->task_id = $request->task_id;
          $cftask->file_path = $file_path; // store path to image to database

          $cftask_add = $cftask->save();
        
        // Get the model of user who assigned the task since we need to send him
        // notification that the task has been completed
        $assigned_by = User::find($task->assign_by);

        /**
        * If the place of work was outside NIB then change the value of location field of 'users'
        * table to NIB now.
        *
        */
        if($task->work_place != 'NIB'){
            $user = User::find($task->assign_to);
            $user->location = 'NIB';
            $user->save();
        }

        // Notify the user who assigned the task that the task has been completed
        $assigned_by->notify(new TaskCompleted($assigned_by));

        /**
        * Redirect to tasks_assigned_to_me once again.
        *  
        */

        if($task_update and $cftask_add)
        return redirect()->route('tasks_assigned_to_me', $request->task_id)->with('success','Congratulations! Your file was uploaded successfully.');
    }
}


     /**
     * Change the uploaded file that was uploaded for submitting tasks
     *
     * @param  $task_id
     * @param  $page_name
     * @return \Illuminate\Http\Response
     *
     */   
     public function change_file($task_id, $page_name) 
     {
        return view('tasks.file.change_file', compact('task_id','page_name'));
     } 

     /**
     * Save uploaded file to drives. Then update the status of the task to 'Complete' and  * save the path to the file to database table.
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */   
     public function store_changed_file(Request $request, $page_name) 
     {
      
      //first delete the existing file from public folder
      $c_f_task = CFTask::where('task_id','=', $request->task_id)->first();
      
      // get public path to current file
      $file_path = storage_path().'app/public'.$c_f_task->file_path;

      
      if(file_exists($file_path)) // check whether file exists or not
        Storage::delete($file_path); // destroy it!!!
      
      /**
      * User will be allowed to upload image, pdf file, word document, excel sheet only.
      */
        
        $this->validate($request, [
            'fileToUpload'=>'required|mimes:jpeg,png,jpg,gif,svg,xls,xlsx,pdf,doc,docx,zip|max:2048'
        ]);
        
        if($request->hasFile('fileToUpload')){
          
          $file = $request->file('fileToUpload');
          $extension = $file->getClientOriginalExtension();

          $file_name = "task_".$request->task_id.".".$extension; // create a file name such as task_1.jpg, task_2.jpg etc.
          
          //$file_path = $request->file('fileToUpload')->storeAs('UploadedImages', $file_name);
          

          /*
          | Use separate folders to store image, word document, excel and pdf file.
          | 
          |
          */ 
          
        if($extension == "doc" or $extension == "docx"){
          $dest_path = storage_path('app/public/words');
          $file_path = "storage/words/".$file_name; // path to the word files in public folder
        }

        elseif($extension == "xls" or $extension == "xlsx"){
          $dest_path = storage_path('app/public/excels');
          $file_path = "storage/excels/".$file_name; // path to the excel sheets in public folder
        }

        elseif($extension == "pdf"){
          $dest_path = storage_path('app/public/pdfs');
          $file_path = "storage/pdfs/".$file_name; // path to the pdf files in public folder
        }

        elseif($extension == "jpeg" or $extension == "png" or $extension == "jpg" or $extension == "gif" or $extension == "svg"){
          $dest_path = storage_path('app/public/images');
          $file_path = "storage/images/".$file_name; // path to the images in public folder
        }
          
          $file->move($dest_path, $file_name);

         
        /**
        * Redirect to the page from where request for file change started
        *  
        */
       
        if($page_name =='self')
        return redirect()->route('self_assigned_tasks', $request->task_id)->with('success','Congratulations! Your file was uploaded successfully.');

      if($page_name =='to_me')
        return redirect()->route('tasks_assigned_to_me', $request->task_id)->with('success','Congratulations! Your file was uploaded successfully.');
    }
}

     /**
     * Download the file that was uploaded as a proof of completing task
     * 
     *
     * @param  $task_id
     * @return \Illuminate\Http\Response
     *
     */  

     public function download_file($c_f_task_id) {
     	$c_f_task = CFTask::find($c_f_task_id);
     	$extension = explode(".",$c_f_task->file_path);
     	$file_name = "task_".$c_f_task->task_id.".".$extension[1];
     	$file_path = storage_path().'/app/public/'.$c_f_task->file_path;

     	return response()->download($file_path, $file_name);
     }
 
}
