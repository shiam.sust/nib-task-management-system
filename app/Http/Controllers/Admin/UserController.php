<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use App\User;
use App\Task;
use App\Models\Division;
use App\Models\Designation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;


class UserController extends Controller
{

    /**
    * Show the list of all junior users in same division as Authenticated user
    *
    * @return Illuminate\Http\Response
    * 
    */

    public function index(){
        // get details about designations of current user
        $user_designation= Designation::find(Auth::user()->designation_id);
        

        // Get the users who are in the same department with the authenticated user 
        // and who are junior in rank than the authenticated user.

        /*
        * Actual SQL query:
        *
        *
        * select `users`.`id` as `id`, `users`.`name` as `user_name`, `divisions`.`name` as 
        * `division`, `designations`.`name` as `designation`, `email`, `mobile` from `users` inner 
        * join `divisions` on `division_id` = `divisions`.`id` inner join `designations` on 
        * `designation_id` = `designations`.`id` where `users`.`division_id` = 
        * user_designation->division_id and `rank` > user_designation->rank order by `rank` asc
        * 
        *
        */

        $users = User::join('divisions', 'division_id','=','divisions.id')
                     ->join('designations','designation_id','=','designations.id')
                     ->where('users.division_id','=',$user_designation->division_id)
                     ->where('rank','>',$user_designation->rank)
                     ->select('users.id as id','users.name AS user_name', 'divisions.name AS division','designations.name AS designation', 'email','mobile', 'location')
                     ->orderBy('rank', 'asc')
                     ->get();
        
        return view('users.view_users_list')->with([
            'users' => $users
        ]);
        //print($users);
    }

    
    /**
    * Show the list of all users of selected division to admin
    *
    * @return Illuminate\Http\Response
    * 
    */

    public function view_users_list_admin($division_id){
      
        // get details about designations of current user
        $user_designation= Designation::find(Auth::user()->designation_id);

        /*
        * Actual SQL query:
        *
        *
        * select `users`.`id` as `id`, `users`.`name` as `user_name`, `divisions`.`name` as 
        * `division`, `designations`.`name` as `designation`, `email`, `mobile` from `users` inner 
        * join `divisions` on `division_id` = `divisions`.`id` inner join `designations` on 
        * `designation_id` = `designations`.`id` where `users`.`division_id` = $division_id
        *  and `rank` > user_designation->rank order by `rank` asc
        * 
        *
        */

        $users = User::join('divisions', 'division_id','=','divisions.id')
                     ->join('designations','designation_id','=','designations.id')
                     ->where('users.division_id','=',$division_id)
                     ->where('rank','>',$user_designation->rank)
                     ->select('users.id as id','users.name AS user_name', 'divisions.name AS division','designations.name AS designation', 'email','mobile', 'location')
                     ->orderBy('rank')
                     ->get();
        
        return view('users.view_users_list')->with([
            'users' => $users
        ]);
        //print($users);
    }


    /**
    * Show users the form for adding new users.
    *
    * @return Illuminate\Http\Response
    */

    public function showUserForm(){
        $divisions = Division::all();
        $designations = Designation::all();
        return view('users.add_user')->with([
            'divisions' => $divisions,
            'designations' => $designations
        ]);
    }
    /**
    * When user select a division Ajax send the selected division to this method. This method 
    * will then return the corresponding designations of the selected division.
    *
    * @param $division_name
    * @return $designation
    *
    */
    public function get_designations(Request $request){
      $division = Division::where('name','=',$request->division_name)->first();
      
      $designations = Designation::where('division_id', '=',$division->id)
                                  ->select('id','name')
                                  ->get();
           
      return $designations;

    }

    public function register(Request $request){
        
        $this->validate($request, array(
            'name' => 'required|max:255',
            'division' => 'required',
            'designation' => 'required',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|confirmed|max:255',
        ));
        //return $request->all();
        //$this->create($request->all());

        $user = new User;

        $user->name = $request->name;
        $user->designation_id = $request->designation;
        $user->division_id = $request->division;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->username = $request->username;
        $user->role = 10;
        $user->is_admin = $request->is_admin;

        //push data to database
        $user->save();

        //redirecting to productlist 
        return redirect()->route('show-users')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created user.'
            ]
        ]);

    }

    /**
    * While registering users after mail verification is complete this function
    * will show the page for uploading profile picture.
    *
    *
    */

    public function upload_profile_pic_1($user_id){

        return view('users.upload_profile_pic_1', compact('user_id'));
    }

    /**
     * Save uploaded file to public folder and redirect to upload_profile_pic_2
     * page to review the image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */   
     public function store_profile_pic_1(Request $request){

        // get the id of last added user
        $user_id = $request->user_id;
        $user = User::find($user_id);
        $this->validate($request, [ 'fileToUpload'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        
        if($request->hasFile('fileToUpload')){
          
          $file = $request->file('fileToUpload');
          $extension = $file->getClientOriginalExtension();

          $file_name = "user_".$user_id.".".$extension; // create a file name such as task_1.jpg, task_2.jpeg etc.

          //$file_path = $request->file('fileToUpload')->storeAs('UploadedImages', $file_name);
          
          
          $dest_path = public_path('/profile_images');
          $file_path = "/profile_images/".$file_name; // path to the images in public folder

          $file->move($dest_path, $file_name);

          $user->image = $file_path;
          $saved = $user->save(); // add path to profile picture to user table
        
        if($saved) // if user table updated successfully 
        return redirect()->route('upload_profile_pic_2', $user_id);
      }
    } 

    /**
    * After profile picture upload this method will take user to upload_profile_pic_2 page.
    * There user will review the profile picture and if necessary user will be able to change
    * it. Otherwise user can finish registration process.
    *
    */

    public function upload_profile_pic_2($user_id){
        $user = User::find($user_id);
        $image = $user->image;
        
        return view('users.upload_profile_pic_2', compact('user_id', 'image'));
    }

    /**
     * First delete current profile picture and then add the new profile picture both in
     * public folder and database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */   
     public function store_profile_pic_2(Request $request) 
     {
        $user = User::find($request->user_id); // get user details
        $current_image = $user->image; // get image path  
        $image_path = explode('/',$current_image); // split the image path on '/'
        $image_name = $image_path[2]; // get image name

        File::delete($image_name); // delete current image

        $this->validate($request, [ 'fileToUpload'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        
        if($request->hasFile('fileToUpload')){
          
          $file = $request->file('fileToUpload');
          $extension = $file->getClientOriginalExtension();

          $file_name = "user_".$request->user_id.".".$extension; // create a file name such as task_1.jpg, task_2.jpeg etc.

          //$file_path = $request->file('fileToUpload')->storeAs('UploadedImages', $file_name);
          
          $dest_path = public_path('/profile_images');
          $file_path = "/profile_images/".$file_name; // path to the images in public folder

          $file->move($dest_path, $file_name);
          //$this->save();

          $user->image = $file_path;
          $saved = $user->save();
        
        if($saved)
        return redirect()->route('upload_profile_pic_2', $user->id);
        
    }
}
    
    public function view_divisions(){
      $users_count = Division::leftJoin('users', 'division_id','=','divisions.id')
                            ->select('divisions.id AS division_id','divisions.name AS division_name', DB::raw('count(users.id) AS users_in_division'))
                            ->groupBy('divisions.id', 'divisions.name')
                            ->orderBy('division_name')
                            ->get();
      return view('users.view_divisions', compact('users_count'));
    }


    public function view_profile($user_id){

      /*
      * Actual SQL query:
      *
      *
      * select `users`.`id` as `id`, `users`.`name` as `user_name`, `divisions`.`name` as 
      * `division`, `designations`.`name` as `designation`, `email`, `mobile` from `users` inner 
      * join `divisions` on `division_id` = `divisions`.`id` inner join `designations` on 
      * `designation_id` = `designations`.`id` where `user_id` = `users`.`id`
      * order by `rank` asc
      *
      */
      
      $user = User::join('divisions', 'division_id','=','divisions.id')
                   ->join('designations','designation_id','=','designations.id')
                   ->where('users.id','=',$user_id)
                   ->select('users.id as id','users.name AS user_name', 'divisions.name AS division','designations.name AS designation', 'email','mobile','location', 'image')
                   ->orderBy('rank')
                   ->first();
      
      $ongoing = Task::where('status','=','Ongoing')
                      ->where('assign_to','=', $user_id)
                      ->count();
      $complete = Task::where('status','=','Completed')
                      ->where('assign_to','=', $user_id)
                      ->count();
      $fail = Task::where('status','=','Failed')
                      ->where('assign_to','=', $user_id)
                      ->count();

      return view('users.view_profile',compact('user','ongoing','complete','fail'));
      
    }
}

