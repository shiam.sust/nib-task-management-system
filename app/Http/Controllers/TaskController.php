<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\CFTask;
use App\DeadlineExtension;
use App\Models\Designation;
use App\Notifications\TaskAssigned;
use Auth;
use DB;

class TaskController extends Controller
{

    /**
     * Display a listing of tasks assigned by user himself
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user_id = Auth::id();
        
        // get all self assigned tasks of user
        $tasks = Task::where([['assign_by','=',$user_id],['assign_to','=',$user_id]])
                       ->orderBy('created_at', 'desc')
                       ->get();

        $pending = Task::where([['assign_by','=',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Pending')
                                ->count();
        $ongoing = Task::where([['assign_by','=',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Ongoing')
                                ->count();
        $completed = Task::where([['assign_by','=',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Completed')
                                ->count();
        $failed = Task::where([['assign_by','=',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Failed')
                                ->count();
        return view('self_assigned_tasks', compact('tasks','pending','ongoing', 'completed','failed'));
    }
    


    /**
     * Display the tasks assigned to current user by senior officers
     *
     * @return \Illuminate\Http\Response
     */
    public function tasks_assigned_to_me()
    {
        $user_id = Auth::id(); 
        /*
        $tasks = Task::where([['assign_by','<>',$user_id],['assign_to','=',$user_id]])
                       ->orderBy('created_at','desc')
                       ->get();
        */

        /**
        * The following query gets tasks assigned by user to himself with name of 
        * the officer  who added the task and the name of the officer to whom the task 
        * was assigned.
        *
        * Actual SQL query:
        * 
        *  SELECT task_id, title, assign_by, assign_to, assign_date, weight, status, dead_line, *  work_place,  assign_by_name, name as assign_to_name FROM (SELECT tasks.id as task_id, *  title, assign_by, assign_to, assign_date, weight, status, dead_line, work_place, name *  as  assign_by_name  FROM tasks, users where assign_by = users.id) U, users as V WHERE *  assign_to = V.id and assign_by <> $user_id and assign_to = $user_id
        * 
        */

        $sub_query = Task::join('users', 'assign_by', '=', 'users.id')
                           ->select('tasks.id AS task_id',   'title', 'assign_by', 'assign_to',      'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'name   AS  assign_by_name');

        $tasks = DB::table(DB::raw("({$sub_query->toSql()}) as sub_query"))
                     ->join('users', 'assign_to', '=', 'users.id')
                     ->where('assign_by','<>',$user_id)
                     ->where('assign_to','=',$user_id)
                     ->select('task_id',   'title', 'assign_by', 'assign_to',      'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'assign_by_name', 'name AS assign_to_name')
                     ->orderBy('created_at', 'desc')
                     ->get();
        
        $pending = Task::where([['assign_by','<>',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Pending')
                                ->count();
        $ongoing = Task::where([['assign_by','<>',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Ongoing')
                                ->count();
        $completed = Task::where([['assign_by','<>',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Completed')
                                ->count();
        $failed = Task::where([['assign_by','<>',$user_id],['assign_to','=',$user_id]])
                                ->where('status','=','Failed')
                                ->count();

        return view('tasks_assigned_to_me', compact('tasks','pending','ongoing', 'completed','failed'));
    }

    /**
     * Display the tasks assigned by authenicated user to his junior officers
     *
     * @return \Illuminate\Http\Response
     */
    public function tasks_assigned_by_me()
    {
        $user_id = Auth::id(); 

        /*
        $tasks = Task::where([['assign_by','=',$user_id],['assign_to','<>',$user_id]])
                       ->orderBy('created_at', 'desc')
                       ->get();
        */

        /**
        * The following query gets tasks assigned by user to himself withe his name  
        * and the name of the officer to whom the task was assigned.
        * 
        *
        * Actual SQL query:
        * 
        *  SELECT task_id, title, assign_by, assign_to, assign_date, weight, status, dead_line, *  work_place,  assign_by_name, name as assign_to_name FROM (SELECT tasks.id as task_id, *  title, assign_by, assign_to, assign_date, weight, status, dead_line, work_place, name *  as  assign_by_name  FROM tasks, users where assign_by = users.id) U, users as V WHERE *  assign_to = V.id and assign_by = $user_id and assign_to <> $user_id
        * 
        */

        $sub_query = Task::join('users', 'assign_by', '=', 'users.id')
            ->select('tasks.id AS task_id',   'title', 'assign_by', 'assign_to',  'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'name   AS  assign_by_name');

        $tasks = DB::table(DB::raw("({$sub_query->toSql()}) as sub_query"))
                     ->join('users', 'assign_to', '=', 'users.id')
                     ->where('assign_by','=',$user_id)
                     ->where('assign_to','<>',$user_id)
                     ->select('task_id',   'title', 'assign_by', 'assign_to',      'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'assign_by_name', 'name AS assign_to_name')
                     ->orderBy('created_at', 'desc')
                     ->get();
        
        $pending = Task::where([['assign_by','=',$user_id],['assign_to','<>',$user_id]])
                                ->where('status','=','Pending')
                                ->count();
        $ongoing = Task::where([['assign_by','=',$user_id],['assign_to','<>',$user_id]])
                                ->where('status','=','Ongoing')
                                ->count();
        $completed = Task::where([['assign_by','=',$user_id],['assign_to','<>',$user_id]])
                                ->where('status','=','Completed')
                                ->count();
        $failed = Task::where([['assign_by','=',$user_id],['assign_to','<>',$user_id]])
                                ->where('status','=','Failed')
                                ->count();
        return view('tasks_assigned_by_me', compact('tasks','pending','ongoing', 'completed','failed'));
    }
    
    /**
     * Display details of selected task
     *
     *
     * @param $task_id 
     *
     * @return Illuminate\Http\Response
     */   
     public function view_task_details($task_id) 
     {
        $task = Task::find($task_id);
        $user_id = Auth::id(); // get Authenticated user id
        
        // if the user to whom the task has been assigned view task details and
        // if task status is currently Pending then change task status to 
        // to Ongoing 
        if($user_id == $task->assign_to and $task->status == 'Pending'){
        $task->status = 'Ongoing';
        $task->save();
        }

        /**
        * The following query gets details of tasks with the name of creator of task and name of
        * officer to whom the task was assigned.
        * 
        *
        * Actual SQL query:
        * 
        *  SELECT task_id, title, assign_by, assign_to, assign_date, weight, status, dead_line, *  work_place,  assign_by_name, name as assign_to_name FROM (SELECT tasks.id as task_id, *  title, assign_by, assign_to, assign_date, weight, status, dead_line, work_place, name *  as  assign_by_name  FROM tasks, users where assign_by = users.id) U, users as V WHERE *  assign_to = V.id
        * 
        */

        $sub_query = Task::join('users', 'assign_by', '=', 'users.id')
                           ->where('tasks.id', '=', $task_id)
            ->select('tasks.id AS task_id',   'title', 'description', 'assign_by', 'assign_to',  'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'edited','tasks.created_at AS task_created_at','tasks.updated_at AS task_updated_at','name   AS  assign_by_name');

        $task = DB::table(DB::raw("({$sub_query->toSql()}) as sub_query"))
                     ->join('users', 'assign_to', '=', 'users.id')
                     ->mergeBindings($sub_query->getQuery())
                     ->select('task_id',   'title', 'description', 'assign_by', 'assign_to',      'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'assign_by_name', 'name AS assign_to_name', 'edited', 'task_created_at', 'task_updated_at')
                     ->orderBy('created_at', 'desc')
                     ->first();
        
        return view('view_task_details', compact('task'));

     }


     /**
     * Display details of selected completed or failed task
     *
     *
     */   
     public function view_c_f_task_details($task_id) 
     {
        $basic_task = Task::find($task_id); // get basic task details

        /* get data related to completed and failed task*/
        $c_f_task = CFTask::where('task_id','=', $task_id) 
                            ->first(); 

        /**
        * The following query gets details of tasks with the name of creator of task and name of
        * officer to whom the task was assigned.
        * 
        *
        * Actual SQL query:
        * 
        *  SELECT task_id, title, assign_by, assign_to, assign_date, weight, status, dead_line, *  work_place,  assign_by_name, name as assign_to_name FROM (SELECT tasks.id as task_id, *  title, assign_by, assign_to, assign_date, weight, status, dead_line, work_place, name *  as  assign_by_name  FROM tasks, users where assign_by = users.id) U, users as V WHERE *  assign_to = V.id
        * 
        */

        $sub_query = Task::join('users', 'assign_by', '=', 'users.id')
                           ->where('tasks.id', '=', $task_id)
            ->select('tasks.id AS task_id',   'title', 'description', 'assign_by', 'assign_to',  'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'edited','tasks.created_at AS task_created_at','tasks.updated_at AS task_updated_at','name   AS  assign_by_name');

        $task = DB::table(DB::raw("({$sub_query->toSql()}) as sub_query"))
                     ->join('users', 'assign_to', '=', 'users.id')
                     ->mergeBindings($sub_query->getQuery())
                     ->select('task_id',   'title', 'description', 'assign_by', 'assign_to',      'assign_date', 'weight', 'status', 'dead_line', 'work_place', 'assign_by_name', 'name AS assign_to_name', 'edited','task_created_at', 'task_updated_at')
                     ->orderBy('created_at', 'desc')
                     ->first();
       // print_r($task);
        //  print_r($c_f_task);
        return view('view_c_f_task_details', compact('task','c_f_task'));

     }
    
    /**
     * Show form for requesting to extend deadline of ongoing task
     *
     *
     */   
     public function request_deadline_extension($task_id) 
     {
        /* Check how many times the deadline of this task has been extended */
        $request_count = DeadlineExtension::where('task_id','=', $task_id)
                                        ->count();
        if($request_count < 3)
            return view('tasks.requests.request_deadline_extension', compact('task_id'));
        else
            return redirect()->back()->with('error', 'Sorry! The deadline for this task cannot be extended anymore.');
     }

     /**
     * Store deadline extension request related data to database 
     *
     *
     */   
     public function store_deadline_extension(Request $request, $task_id) 
     {
        $deadline_extension = new DeadlineExtension;
        $deadline_extension->task_id = $task_id;
        $deadline_extension->expect_deadline = $request->expect_deadline;
        $deadline_extension->message = $request->message;
        $deadline_extension->created_at = now();
        $deadline_extension->updated_at = now();

        $saved = $deadline_extension->save();

        if($saved)
        return redirect()->route('tasks_assigned_to_me')->with('success','Your request was sent successfully!');
     }

     /**
     * Show deadline extension request with necessary task details of ongoing task and
     * form for extending deadline
     *
     */   
     public function view_deadline_extension_request($task_id) 
     {
        /* Get the details about last deadline extension request of given task */
        $request = DeadlineExtension::where('task_id','=', $task_id)
                                        ->orderBy('created_at', 'desc')
                                        ->first();

        $task = Task::find($task_id); // Get task details
        /* Get details about the user to whom the task has been assigned */
        $user = User::find($task->assign_to);
        
        return view('tasks.requests.view_deadline_extension_request', compact('request','task','user'));
     }

     /**
     * Extend the deadline of requested ongoing task
     *
     *
     */
    public function extend_deadline(Request $request, $task_id) 
     {
        $task = Task::find($task_id); // Get task details
        $task->dead_line = $request->deadline;

        $saved = $task->save();
        
        if($saved)
        return redirect()->route('tasks_assigned_to_me')->with('success', 'Deadline extended successfully.');
     }
    
    /**
     * Show form for requesting to extend deadline of ongoing self assigned task
     *
     *
     */   
     public function request_self_deadline_extension($task_id) 
     {
        /* Check how many times the deadline of this task has been extended */
        $request_count = DeadlineExtension::where('task_id','=', $task_id)
                                        ->count();
        $task = Task::find($task_id); // get details about self assigned tasks

        if($request_count < 3)
            return view('tasks.requests.request_self_deadline_extension', compact('task'));
        else
            return redirect()->back()->with('error', 'Sorry! The deadline for this task cannot be extended anymore.');
     }
    
    /**
     * Store deadline extension request related data to database 
     *
     *
     */   
     public function store_self_deadline_extension(Request $request, $task_id) 
     {
        $deadline_extension = new DeadlineExtension;
        $deadline_extension->task_id = $task_id;
        $deadline_extension->expect_deadline = $request->expect_deadline;
        $deadline_extension->message = $request->message;
        $deadline_extension->created_at = now();
        $deadline_extension->updated_at = now();

        $request_saved = $deadline_extension->save();

        $task = Task::find($task_id); // Get task details
        $task->dead_line = $request->deadline;

        $task_saved = $task->save();
        
        if($request_saved and $task_saved)
        return redirect()->route('self_assigned_tasks')->with('success', 'Deadline extended successfully.');
     }



/**
 * Show form for remarking on a completed task or write reason for failed task.
 *
 * @param task_id
 * @param page_name From which page (i.e. 'self_assigned_tasks' or 'tasks_assigned_by_me') we 
 *        have come to this page.
 */   
 public function remark_task($task_id, $page_name) 
 {
    /**
    |=======================================
    | Explanation
    |=======================================
    | We need to know status of task. Because if the task is completed then in form
    | we will show the 'Mark' and 'Comment' field. But if task is failed then we will show 
    | 'Reason' field only. And to know task status we need to get basic details through
    | 'Task' model and send basic details as parameter to the view.
    */

    $task = Task::find($task_id); // get basic details of task
    $c_f_task = CFTask::where('task_id', '=', $task_id)->first(); // get current remark on the task

    return view('tasks.c_f_tasks.remark_task', compact('task','c_f_task', 'page_name'));
 } 
  
/**
* Store remarks on completed or reason of failed tasks
*
*
*/   
public function store_remark(Request $request, $task_id, $page_name) 
{
    // get current remark on the task
    $c_f_task = CFTask::where('task_id','=',$task_id)->first(); 
    $task = Task::find($task_id); // get basic task details

    if($task->status == "Completed"){
        $c_f_task->mark = $request->mark;
        $c_f_task->comment = $request->comment;
    }

    elseif($task->status == "Failed"){
        $c_f_task->explain = $request->explain;
    }
    
    $saved = $c_f_task->save();
    
    if($saved){
        if($page_name=="self")
        return redirect()->route('self_assigned_tasks')->with('success','Database was updated successfully.');
        elseif($page_name=="by_me")
        return redirect()->route('tasks_assigned_by_me')->with('success','Database was updated successfully.');
        elseif($page_name=="to_me")
        return redirect()->route('tasks_assigned_to_me')->with('success','Database was updated successfully.');
    }
}   

/**
 * View form for adding new task
 *
 * 
 * @return \Illuminate\Http\Response
 */

    function add_task(){

        $user = Auth::user(); // get all details of Authenticated user
        /*
        Get details about user's designation including the rank of the designation
        */
        $user_designation = Designation::find($user->designation_id); 
        
        /*
        If user is not admin then show him the list of his junior users from his department only */

        if($user->is_admin == 0){
        /**
        * The following query gets necessary details of the junior officers of same department 
        * as the Authenticated user whom the Authenticated user is allowed to assign task.
        * 
        * Actual SQL Query:
          select * from (SELECT users.id as user_id, designations.id as designation_id__, users.name as user_name, designations.name as designation_name, rank FROM users, designations WHERE designation_id=designations.id and designations.division_id = 6) R where R.rank > 3
        *
        *
        */
        
        $designations = User::join('designations', 'designation_id','=','designations.id')
                              ->where('designations.division_id','=',$user->division_id)
                              ->select('users.id AS user_id', 'designations.id AS designation_id_', 'users.name AS user_name', 'designations.name AS designation_name', 'rank', 'location');
         /*
         Query builder is used for this query since the current table was created through a sub-query in previous line and it has no corresponding Model.
         */

         
        $officers = DB::table(DB::raw("({$designations->toSql()}) as designations"))
                               ->mergeBindings($designations->getQuery())   
                               ->where('rank', '>', $user_designation->rank)
                               ->orderBy('rank', 'asc')
                               ->get();
        }
        
        /*
          If user is admin then show him the list of all officers of NIB.
        */
        else{
            /**
        * The following query gets necessary details of the junior officers of all the officers 
        * except admin
        * 
        * Actual SQL Query:
          select * from (SELECT users.id as user_id, designations.id as designation_id__, users.name as user_name, designations.name as designation_name, rank FROM users, designations WHERE designation_id=designations.id) R where R.rank > 3
        *
        *
        */
        
        $designations = User::join('designations', 'designation_id','=','designations.id')
                              ->select('users.id AS user_id', 'designations.id AS designation_id_', 'users.name AS user_name', 'designations.name AS designation_name', 'rank', 'location');
         /*
         * Query builder is used for this query since the current table was created       * through a sub-query in previous line and it has no corresponding Model.
         */

         
        $officers = DB::table(DB::raw("({$designations->toSql()}) as designations"))
                               ->mergeBindings($designations->getQuery())   
                               ->where('rank', '>', $user_designation->rank)
                               ->orderBy('rank','asc')
                               ->get();
        }
        
        return view('add_task', compact('officers'));
    }

    /**
     * Store the new task to database after proper validation and send notification to 
     * user tha he has been assigned a new task
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth_user = Auth::id();
        $task = new Task;
        $assign_date = date('Y-m-d'); // get todays date as date of task assignment
        $validateData = $request->validate([
            'title' => 'required|max:255',
            'assign_to'=>'required',
            'weight' => 'required|numeric',
            'dead_line' => 'date|after_or_equal:'.$assign_date,
            'work_place' => 'required'  
        ]);

        /*
        * $request->assign_to format: user_name(user_id)
        * Firstly $request->assign_to is split into 'user_name' and 'user_id)' using preg_split * method on '('.
        * Then 'user_id)' is split using preg_split method on ')' which in turn returns 'user_id'
        * only.
        */
        
        /*
        $user = preg_split("/\(/", $request->assign_to);
        $assign_to = preg_split("/\)/", $user[1]);
        */
        /*
        Since preg_split returns an array convert it to string using implode method
        */
        /*
        $assign_to = implode("",$assign_to); 
        */

        /**
        | UPDATE: Now we are showing the name of officer to the user and storing the user id in
        | value attribute of the input field. So the user id in value attribute will be saved to 
        | to database while user can easily select their suitable officer from the list of the 
        | names. Moreover, we are sending the location of the user to indicate which officers are 
        | currently outside of NIB.
        |
        */

        
        $task->title = $request->title;
        $task->description = $request->description;
        $task->assign_by = $auth_user;
        $task->assign_to = $request->assign_to;
        $task->assign_date = $assign_date;
        $task->weight = $request->weight;
        
        if($task->assign_to == $task->assign_by)
        $task->status = "Ongoing";
        
        elseif($task->assign_to != $task->assign_by)
        $task->status = "Pending";

        $task->dead_line= $request->dead_line;
        $task->work_place = $request->work_place;
        $task->created_at= now();
        $task->updated_at= now();
        
        $saved = $task->save();

        /**
        * If the place of work is outside NIB then change the value of location field of 'users'
        * table to place of work.
        *
        */

        if($task->work_place != 'NIB'){
            $user = User::find($task->assign_to);
            $user->location = $task->work_place;
            $user->save();
        }
        
        
        if($saved){
            $assigned_by = Auth::user(); // get details about user who created the task
            //get details about user to whom task has been assigned
            $assigned_to = User::find($request->assign_to);
            
            // notify user that he has been assigned a new task through TaskAssigned class
            // only if it is not a self assigned task
            if($assigned_to->id != $assigned_by->id)
            $assigned_to->notify(new TaskAssigned($assigned_to));
            
            // return to the add_task page once again with success message after adding task 
            return redirect()->back()->with('success', 'New task was added successfully.');
        }
        elseif(!$saved)
            return redirect()->back()->with('error', 'Whoops! There were some problems while adding the task.');
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $task = Task::find($id);

       $user = Auth::user(); // Get details of Authenticated user

       /* Get the details of officer to whom the task is currently assigned */
       $assign_to = User::find($task->assign_to); 
       
       /* 
       Get details about Authenticated user's designation including the rank of the designation */
       $user_designation = Designation::find($user->designation_id); 


       if($user->is_admin()==0){
       /**
        * The following query gets necessary details of the junior officers of same department 
        * as the Authenticated user whom the Authenticated user is allowed to assign task.
        * 
        * Actual SQL Query:
          select * from (SELECT users.id as user_id, designations.id as designation_id__, users.name as user_name, designations.name as designation_name, rank FROM users, designations WHERE designation_id=designations.id and designations.division_id = users.division_id) R where R.rank > rank
        *
        *
        */
        
        $designations = User::join('designations', 'designation_id','=','designations.id')
                              ->where('designations.division_id','=',$user->division_id)
                              ->select('users.id AS user_id', 'designations.id AS designation_id_', 'users.name AS user_name', 'designations.name AS designation_name', 'rank', 'location');
         /*
         Query builder is used for this query since the current table was created through a sub-query in previous line and it has no corresponding Model.
         */

        $officers = DB::table(DB::raw("({$designations->toSql()}) as designations"))
                               ->mergeBindings($designations->getQuery())   
                               ->where('rank', '>', $user_designation->rank)
                               ->orderBy('rank', 'asc')
                               ->get();
        }

        /*
        * If user is admin then show him the list of all officers of NIB.
        */
        else{
        /**
        * The following query gets necessary details of the junior officers of all the officers 
        * except admin
        * 
        * Actual SQL Query:
          select * from (SELECT users.id as user_id, designations.id as designation_id__, users.name as user_name, designations.name as designation_name, rank FROM users, designations WHERE designation_id=designations.id) R where R.rank > 3
        *
        *
        */
        
        $designations = User::join('designations', 'designation_id','=','designations.id')
                              ->select('users.id AS user_id', 'designations.id AS designation_id_', 'users.name AS user_name', 'designations.name AS designation_name', 'rank', 'location');
         /*
         Query builder is used for this query since the current table was created through a sub-query in previous line and it has no corresponding Model.
         */

         
        $officers = DB::table(DB::raw("({$designations->toSql()}) as designations"))
                               ->mergeBindings($designations->getQuery())   
                               ->where('rank', '>', $user_designation->rank)
                               ->orderBy('rank','asc')
                               ->get();
        }

        /**
        * Now create an array of the name and id of junior officers of Authenticated user in 
        * same department as Authenticated user and keep the name of officer to whom the task is
        * currently assigned at the top. Here note that we have kept officer id in brace since
        * database table of task we will store the id of the officer not the name. But we need to
        * show the user the name of officers. So we kept both name and officer id. While updating
        * database table we will extract the selected user id as done in add_task method using 
        * regex and store it in database table for tasks.
        */
        
        /*
        $index = 1;
        $junior_officers[0] = $assign_to->name."(".$assign_to->id.")";
        if($assign_to->id != $user->id){ // for self_assigned_tasks
        $junior_officers[$index] = $user->name."(".$user->id.")";
        $index += 1;
        }
        */

        /**
        | UPDATE: Now we are showing the name of officer to the user and storing the user id in
        | value attribute of the input field. So the user id in value attribute will be saved to 
        | to database while user can easily select their suitable officer from the name of the 
        | list. Moreover, we are sending the location of the user to indicate which officers are 
        | currently outside of NIB.
        |
        */
        $index = 1;
        $junior_officers_name[0] = $assign_to->name;
        $junior_officers_id[0] = $assign_to->id;
        $junior_officers_location[0] = $assign_to->location;

        if($assign_to->id != $user->id){ // for self_assigned_tasks
        $junior_officers_name[$index] = $user->name;
        $junior_officers_id[$index] = $user->id;
        $junior_officers_location[$index] = $user->location;
        $index += 1;
        }
      

        foreach($officers as $officer){
            if($officer->user_id != $assign_to->id and $officer->user_id != $user->id){
              // $junior_officers[$index] = $officer->user_name."(".$officer->user_id.")";
                $junior_officers_name[$index] = $officer->user_name;
                $junior_officers_id[$index] = $officer->user_id;
                $junior_officers_location[$index] = $officer->location;
                $index++;
            }
         }

         $name_len = count($junior_officers_name);

       return view('edit_task', compact('task', 'junior_officers_name', 'junior_officers_id', 'junior_officers_location','name_len'));
    }

    /**
     * Update the task details in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        $validateData = $request->validate([
            'title' => 'required|max:255',
            'assign_to'=>'required',
            'weight' => 'required|numeric',
            'assign_date' => 'date',
            'dead_line' => 'date|after_or_equal:assign_date',
            'work_place' => 'required'  
        ]);

        /*
        * $request->assign_to format: user_name(user_id)
        * Firstly $request->assign_to is split into 'user_name' and 'user_id)' using preg_split * method on '('.
        * Then 'user_id)' is split using preg_split method on ')' which in turn returns 'user_id'
        * only.
        */
        

        /*
        $user = preg_split("/\(/", $request->assign_to);
        $assign_to = preg_split("/\)/", $user[1]);
        */

        /*
        Since preg_split returns an array convert it to string using implode method
        */

        /*
        $assign_to = implode("",$assign_to); 
        */

        $task->title =$request->title;
        $task->description =$request->description;
        $task->assign_by = $task->assign_by;
        $task->assign_to = $request->assign_to;
        $task->assign_date = $request->assign_date;
        $task->weight =$request->weight;
        
        /* For self assigned tasks deadline can be changed only through requesting */
        if($task->assign_by != $task->assign_to)
        $task->dead_line = $request->dead_line;

        $task->work_place = $request->work_place;
        $updated_at = now();
        
        $saved = $task->save();

        /**
        * Set the location of the user according to the place of work i.e if work place is 
        * outside NIB then set the exact place of work in users location or if work_ lace
        * is inside NIB then set user location to NIB.
        *
        */
        
        $user = User::find($task->assign_to);
        $user->location = $task->work_place;
        $user->save();
        
        if($saved)
            return redirect()->route('self_assigned_tasks')->with('success', 'The task was updated successfully!');
        elseif(!$saved)
            return redirect()->back()->with('error', 'Whoops! There were some problems while updating the task.');
    }

    /**
     * Display a listing of tasks assigned by user himself
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_task($task_id, $page_name)
    {
        
        $task = Task::find($task_id);
        $assign_to = User::find($task->assign_to);
        $deleted = $task->delete(); // delete task
        
        // If any task with workplace outside of NIB is deleted then set the location of the officer to whom the task was assigned to NIB.
        if($task->work_place != 'NIB'){
            $assign_to->location = 'NIB';
            $assign_to->save();
        }

        if($deleted){
            if($page_name == "self")
                return redirect()->route('self_assigned_tasks')->with('success','Task was deleted successfully.');
            elseif($page_name == "by_me")
                return redirect()->route('tasks_assigned_by_me')->with('success','Task was  deleted successfully.');
        }
    }    

public function valid_tasks(){
    $today = date('Y-m-d');
    $tasks = Task::WhereDate('dead_line','<', $today)
                   ->where(function ($query) {
                        $query->where('status', 'Pending')
                        ->orWhere('status', 'Ongoing');
                    })
                    ->get();
        $index = 0;
        foreach($tasks as $task){
            $task->status = 'Failed';
            $saved = $task->save();
            if($saved)
                print("Failed successfully!!!");
            if($index >= 3)
                break;
            $index+=1;
        }
}

}
