<?php

namespace App\Http\Controllers\Auth;

// These two are used to override the 'register' method of 'RegeisterUsers' trait
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;


use App\User;
use App\Models\Division;
use App\Models\Designation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/upload_profile_pic_1';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('guest');
    }

    /**
     * Show the application registration form. This method is going to override the method with
     * same name at RegistersUsers trait. Because with that method it impossible to send 
     * list of existing divisons and designation. So the method was overrided here.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $divisions = Division::all();
        $designations = Designation::all();
        return view('auth.register', compact('divisions', 'designations'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'division' => ['required', 'not_regex:/Choose Division/'],  
            'designation' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'division_id' => $data['division'],
            'designation_id' => $data['designation'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'role' => 10,
            'is_admin' => $data['is_admin'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect()->route('upload_profile_pic_1',['user_id'=>$user->id]);
    }

    
}
