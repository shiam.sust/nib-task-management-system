<?php

namespace App\Http\Controllers\Auth;

// The following three were used to override the two methods named 'show' and 'verify' of
// 'VerifyEmails' trait
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Access\AuthorizationException;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = 'upload_profile_pic_1';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * Show the email verification notice. The method is overring the method in VeriifyEmails
     * trait. Because we need to redirect to upload_profile_pic_1 view with user_id as parameter.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // this part was updated to redirect with parameter
        return $request->user()->hasVerifiedEmail()
                       ? redirect()->route($this->redirectPath(),['user_id'=>$request->user()->id])
                       : view('auth.verify');        
    }

    /**
     * Mark the authenticated user's email address as verified. The method is overring the method 
     * in VeriifyEmails trait. Because we need to redirect to upload_profile_pic_1 view with 
     * user_id as parameter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function verify(Request $request)
    {
        if ($request->route('id') != $request->user()->getKey()) {
            throw new AuthorizationException;
        }
        
        // this part was updated to redirect with parameter
        if ($request->user()->hasVerifiedEmail()) { 
            return redirect()->route($this->redirectPath(),['user_id'=>$request->user()->id]);
        }

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        // this part was updated to redirect with parameter
        return redirect()->route($this->redirectPath(),['user_id'=>$request->user()->id]);
    }
}
