<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
// view add new task form
Route::get('/add_task', 'TaskController@add_task')->name('add_task');

// add tha newly added task data to db
Route::post('/add_task', 'TaskController@store');


Route::get('/self_assigned_tasks', 'TaskController@index')->name('self_assigned_tasks');

Route::get('/tasks_assigned_to_me', 'TaskController@tasks_assigned_to_me')->name('tasks_assigned_to_me');

Route::get('/tasks_assigned_by_me', 'TaskController@tasks_assigned_by_me')->name('tasks_assigned_by_me');
*/

Route::get('/login', function () {
    return view('login');
});

Route::get('/add_user', function () {
    return view('add_user');
})->name('add_user');

Route::get('/get_designations', 'Admin\UserController@get_designations')->name('get_designations');

Auth::routes(['verify' => true]);

// view form for uploading profile picture
Route::get('/upload_profile_pic_1/{user_id}', 'Admin\UserController@upload_profile_pic_1')->middleware('verified')->name('upload_profile_pic_1');
// store image to public folder and image path to database
Route::post('/store_profile_pic_1', 'Admin\UserController@store_profile_pic_1')->name('store_profile_pic_1');

// view form for previewing and changing profile picture
Route::get('/upload_profile_pic_2/{user_id}', 'Admin\UserController@upload_profile_pic_2')->name('upload_profile_pic_2');
// store image to public folder and image path to database
Route::post('/store_profile_pic_2', 'Admin\UserController@store_profile_pic_2')->name('store_profile_pic_2');

Route::get('/home', 'HomeController@index')->name('home');

                                   
Route::post('/login/custom', [
        'uses'  => 'LoginController@login',
        'as'    => 'login.custom'
    ]);

Route::group(['middleware' => 'auth'], function(){
    
    // just for code test
    Route::get('/valid_tasks', 'TaskController@valid_tasks')->name('valid_tasks');

	Route::get('/dashboard', function () {
	    return view('central-admin.dashboard');
	})->name('dashboard');
    
    // when user click on any pending tasks in notification then show him/her all 
    // pending tasks not viewd till then
    Route::get('/view_pending_tasks','NotificationController@view_pending_tasks')->name('view_pending_tasks');

    // when user click on any completed tasks in notification then show him/her all 
    // completed tasks not viewd till then
    Route::get('/view_comleted_tasks','NotificationController@view_completed_tasks')->name('view_completed_tasks');

	Route::get('/', function () {
	    return redirect()->route('self_assigned_tasks');
    });
    
    Route::get('/show-users', [
        'uses'  => 'Admin\UserController@index',
        'as'    => 'show-users'
    ]);
    
    // show form to add new user
     Route::get('/add-users', [
        'uses'  => 'Admin\UserController@showUserForm',
        'as'    => 'add-users'
    ]);

    Route::post('/add-users', [
        'uses'  => 'Admin\UserController@register',
        'as'    => 'add-users'
    ]);
    
    // collect the newly generated notifications to show user
    Route::get('/get_notifications','NotificationController@get_notifications')->name('get_notifications');

    // view divsions of NIB 
    Route::get('/view_divisions', 'Admin\UserController@view_divisions')->name('view_divisions');

    // view junior users list of current users
    Route::get('/view_users_list', 'Admin\UserController@index')->name('view_users_list');

    // view all users list for admin
    Route::get('/view_users_list_admin/{division_id}', 'Admin\UserController@view_users_list_admin')->name('view_users_list_admin');

    // view profile details 
    Route::get('/view_profile/{user_id}', 'Admin\UserController@view_profile')->name('view_profile');

    // view add new task form
    Route::get('/add_task', 'TaskController@add_task')->name('add_task');

    // add tha newly added task data to db
    Route::post('/add_task', 'TaskController@store');

    Route::get('/self_assigned_tasks', 'TaskController@index')->name('self_assigned_tasks');

    Route::get('/tasks_assigned_to_me', 'TaskController@tasks_assigned_to_me')->name('tasks_assigned_to_me');

    Route::get('/tasks_assigned_by_me', 'TaskController@tasks_assigned_by_me')->name('tasks_assigned_by_me');
    
    // view form for editing existing task
    Route::get('/edit_task/{task_id}', ['uses'=>'TaskController@edit', 'as'=>'edit_task']);

    // update data of existing task in database
    Route::post('/edit_task/{task_id}', ['uses'=>'TaskController@update']);
    
    // view details of selected existing task
    Route::get('/view_task_details/{task_id}', 'TaskController@view_task_details')->name('view_task_details'); 

    // view details of selected existing completed or failed task
    Route::get('/view_c_f_task_details/{task_id}', 'TaskController@view_c_f_task_details')->name('view_c_f_task_details'); 
    
    // send request for extending deadline of an ongoing task
    Route::get('request_deadline_extension/{task_id}', 'TaskController@request_deadline_extension')->name('request_deadline_extension'); 

    // insert request for deadline extension related data to database
    Route::post('request_deadline_extension/{task_id}', 'TaskController@store_deadline_extension'); 
    
    // show deadline extension request and update deadline if user want
    Route::get('view_deadline_extension_request/{task_id}', 'TaskController@view_deadline_extension_request')->name('view_deadline_extension_request');

    // extend deadline of ongoig task as per the request
    Route::post('extend_deadline/{task_id}', 'TaskController@extend_deadline')->name('extend_deadline');

    // send request for extending deadline of an ongoing selfassigned task
    Route::get('request_self_deadline_extension/{task_id}', 'TaskController@request_self_deadline_extension')->name('request_self_deadline_extension'); 

    // insert request for deadline extension related data to database
    Route::post('request_self_deadline_extension/{task_id}', 'TaskController@store_self_deadline_extension'); 

    // upload file from selft_assigned_tasks as proof of task completion and redirect to self_assigned_tasks page
    Route::get('/upload_file_self/{task_id}', 'FileController@upload_file_self')->name('upload_file_self');
    
    // store file to database for self_assigned_tasks page
    Route::post('/store_image_self', 'FileController@store_image_self')->name('store_image_self');  

    // upload file from tasks_assigned_to_me as proof of task completion and redirect to tasks_assigned_to_me page
    Route::get('/upload_file_authority/{task_id}', 'FileController@upload_file_authority')->name('upload_file_authority');

    // store file to database for tasks_assigned_to_me
    Route::post('/store_image_authority', 'FileController@store_image_authority')->name('store_image_authority');  
    
    // show user page for changing uploaded file
    Route::get('/change_file/{task_id}/{page_name}', 'FileController@change_file')->name('change_file');

    // save the changed file to public folder and update file_path in database
    Route::post('/store_changed_file/{page_name}', 'FileController@store_changed_file')->name('store_changed_file');

    // download the file that was uploaded as proof of completion of task
    Route::get('/download_file/{c_f_task_id}', 'FileController@download_file')->name('download_file');  

    //show form for remarking on completed task or writing reason for failed task
    Route::get('/remark_task/{task_id}/{page_name}', 'TaskController@remark_task')->name('remark_task');
    
    /**
    * Store remark on completed task or  reason for failed task in database
    * Page name parameter indicates from which page the request has come i.e. 
    * self_assigned_tasks page or tasks_assigned_by_me page.
    * Because after storing data to database we will redirect to the page from which 
    * request had come.
    */
    Route::post('/store_remark/{task_id}/{page_name}', 'TaskController@store_remark')->name('store_remark');
    
    // delete task
    Route::get('/delete_task/{task_id}/{page_name}', 'TaskController@delete_task')->name('delete_task');

    Route::get('logout',function (){
        Auth::logout();
        return redirect()->route('login');
    })->name('logout');

});