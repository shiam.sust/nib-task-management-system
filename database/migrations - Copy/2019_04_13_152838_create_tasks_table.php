<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->mediumText('description');
            $table->integer('assign_by')->unsigned();
            $table->integer('assign_to')->unsigned();
			$table->date('assign_date');
			$table->integer('weight');
            $table->string('status');
            $table->date('dead_line');
            $table->string('work_place');
            $table->timestamps();
        });
        /*
        Schema::table('tasks' function($table){
            $table->integer('mark');
            $table->text('comment');
            $table->string('file_path');
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
