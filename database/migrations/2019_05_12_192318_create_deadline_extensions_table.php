<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeadlineExtensionsTable extends Migration
{
    /**
     * Store necessary details about date extension request.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deadline_extensions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('task_id')->unsigned();
            $table->date('expect_deadline');
            $table->mediumText('message')->nullable();
            $table->timestamps();
        });

        Schema::table('deadline_extensions', function ($table) {
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deadline_extensions');
    }
}
