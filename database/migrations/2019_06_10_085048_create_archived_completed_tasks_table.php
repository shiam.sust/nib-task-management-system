<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivedCompletedTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archived_completed_tasks', function (Blueprint $table) {
            $table->bigInteger('task_id')->unsigned();
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->bigInteger('assign_by')->unsigned();
            $table->bigInteger('assign_to')->unsigned();
            $table->date('assign_date');
            $table->integer('weight');
            $table->string('status');
            $table->date('dead_line');
            $table->string('work_place');
            $table->integer('mark')->nullable();
            $table->mediumText('comment')->nullable();
            $table->string('file_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archived_completed_tasks');
    }
}
