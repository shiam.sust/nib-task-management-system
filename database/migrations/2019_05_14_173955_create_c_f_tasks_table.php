<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCFTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_f_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('task_id')->unsigned();
            $table->integer('mark')->nullable();
            $table->mediumText('comment')->nullable();
            $table->mediumText('explain')->nullable();
            $table->string('file_path')->nullable();
            $table->timestamps();
        });

        Schema::table('c_f_tasks', function($table){
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_f_tasks');
    }
}
