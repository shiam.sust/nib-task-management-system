<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivedFailedTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archived_failed_tasks', function (Blueprint $table) {
            $table->bigInteger('task_id')->unsigned();
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->bigInteger('assign_by')->unsigned();
            $table->bigInteger('assign_to')->unsigned();
            $table->date('assign_date');
            $table->integer('weight');
            $table->string('status');
            $table->date('dead_line');
            $table->string('work_place');
            $table->mediumText('explain')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archived_failed_tasks');
    }
}
