<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('username')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('division_id')->unsigned();
            $table->integer('designation_id')->unsigned();
            $table->string('mobile')->nullable();
            $table->tinyInteger('role')->nullable();
            $table->boolean('is_admin')->default(1);;
            $table->rememberToken();
            $table->timestamps();

        });

        Schema::table('users', function($table){
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade');
            $table->foreign('designation_id')->references('id')->on('designations')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
