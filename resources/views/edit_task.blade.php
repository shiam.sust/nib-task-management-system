@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System Edit Task </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>

<script>

	/* start - add task type options to 'title' field excluding the one that has come from database. Currently in no use.*/

	window.addEventListener('load', 
	function (){
	var taskType = ['Research', 'Management', 'Test',];
	var selectInput = document.getElementById('title');
	var selectedTask = document.getElementById('hidden_title').value;
	for(var i = 0; i < taskType.length; ++i){
		if(taskType[i] != selectedTask){
			var optionInput = document.createElement('option');
			optionInput.appendChild(document.createTextNode(taskType[i]));
			selectInput.appendChild(optionInput);

		}
	}
});
/*end*/

/* start - add weight as options to 'weight' field excluding the one that has come from database */
    window.addEventListener('load',
    function(){
	var weights = ['10', '20', '30', '40', '50', '60', '70', '80', '90', '100'];
	var selectInput = document.getElementById('weight');
	var selectedWeight = document.getElementById('hidden_weight').value;
	for(var i = 0; i < weights.length; ++i){
		if(weights[i] != selectedWeight){
			var optionInput = document.createElement('option');
			optionInput.appendChild(document.createTextNode(weights[i]));
			selectInput.appendChild(optionInput);

		}

	}
});
/*end*/

/* start - add options to 'work_place' excluding the one that has come from database*/

	window.addEventListener('load',
    function(){
	var place = ['NIB', 'Outside NIB'];
	var selectInput = document.getElementById('work_place');
	var selectedPlace = document.getElementById('hidden_place').value;
	for(var i = 0; i < place.length; ++i){
		if(place[i] != selectedPlace){
			var optionInput = document.createElement('option');
			optionInput.appendChild(document.createTextNode(place[i]));
			selectInput.appendChild(optionInput);

		}

	}
});
/*end*/

</script>
</head>
@endsection

@section('content')
    <div class="container-fluid mt-5">
	<div class="login-form">
		@if ($message = Session::get('error'))
    
      <div class="alert alert-danger alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>

@endif

	<div class = "page-title">
		<h1 class="title text-center">Edit Task</h1>
	</div>
    
    <div id="input-field">
		<form id="login-form" method="POST" class="form-signin" role="form" action="{{ route('edit_task', $task->id) }}">
        
		@csrf <!-- CSRF validation -->

	    <!--Title-->
		<div class="form-group">
			<label for="title"> Title:</label>
			<input name="title" id="title" class="form-control" place="Title" value="{{ $task->title }}" >
			<input id="hidden_title" type="hidden" value="{{ $task->title }}">

			@if ($errors->has('title'))
            <div class="error text-danger">{{ $errors->first('title') }}</div>
			@endif
		</div>

		<!--Description-->
        <div class="form-group">
        	<label for="description"> Description: </label>
        	<textarea name = "description" id="description" class="form-control" rows="5" > {{ $task->description }} </textarea>
		</div>

		<!--Assign to-->
        <div class="form-group">
        	<label for="assign_to"> Assign To:</label>
			<select name="assign_to" id="assign_to" class="form-control">
				{{ $index = 0 }}
				@for ($index = 0; $index < $name_len; ++$index)
				
				@if($junior_officers_location[$index] == 'NIB')
				<option value="{{ $junior_officers_id[$index] }}">{{ $junior_officers_name[$index] }}</option>
                     
                @elseif($junior_officers_location[$index] != 'NIB')
				<option value="{{ $junior_officers_id[$index] }}">{{ $junior_officers_name[$index] }}(Outside)</option>
				@endif
               
				@endfor
			</select>
		</div>

		<!--Assign date-->
        <div class="form-group">
        	<label for="assign_date"> Assignment Date:</label>
			<input name="assign_date" id="assign_date" type="date" class="form-control" value="{{ $task->assign_date }}" > 
		</div>
        
        <!--Weight-->
        <div class="form-group">
        	<label for="weight"> Weight:</label>
			<select name="weight" id="weight" class="form-control">
				<option>{{ $task->weight }}</option>
			</select>
			<input id="hidden_weight" type="hidden" value="{{ $task->weight }}">
		</div>
		
		<!--Deadline-->
		@if($task->assign_by != $task->assign_to)
        <div class="form-group">
        	<label for="dead_line"> Deadline:</label>
			<input name="dead_line" id="dead_line" type="date" class="form-control" value="{{ $task->dead_line }}" > 

            @if ($errors->has('dead_line'))
            <div class="error text-danger">{{ $errors->first('dead_line') }}</div>
			@endif

		</div>
		@endif

        <!--Work Place-->
		<div class="form-group">
			<label for="location"> Place of Work<span class="text-danger m-l-5">*</span></label>
			<div class="radio-list">
                <label class="radio-inline p-4">
                    <div class="radio radio-info">
                        <input type="radio" name="location" id="radio1" value="NIB" >
                        <label for="radio1">Inside NIB</label>
                    </div>
                </label>
                <label class="radio-inline">
                    <div class="radio radio-info">
                        <input type="radio" name="location" id="radio2" value="Outside NIB">
                        <label for="radio2">Outside NIB </label>
                    </div>
                </label>
            </div>
            @if ($errors->has('work_place'))
            <div class="error text-danger">{{ $errors->first('work_place') }}</div>
			@endif
		</div>

		<div class="form-group" id="work_place_div">
        	<label for="work_place">Exact Place of Work<span class="text-danger m-l-5">*</span></label>
			<input name="work_place" id="work_place" type="text" class="form-control" placeholder="Exact location of work" > 
		</div>
			<button class="btn btn-block bt-login" type="submit"><h4>Update</h4></button>
		</form>
	</div>
	</div>
   </div>

   <script type="text/javascript">
    $(document).ready(function(){
    	$("#work_place_div").hide();
        $('input[type="radio"]').change(function(){
            if($("#radio1").is(":checked")){
            	$("#work_place").val("NIB");
                $("#work_place_div").hide();
            }
            else if($("#radio2").is(":checked")){
            	$("#work_place").val("");
                $("#work_place_div").show();
            }
        });
       });
	</script>
@endsection