@extends('layouts.master')

@section('head')
@parent

<title>NIB user Management System User Home </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>

<style>
  .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

  
}
</style>
</head>
@endsection

@section('content')   
<!--inner block start here-->
<div class="inner-block">
  
   @if ($message = Session::get('success'))
    
      <div class="alert alert-success alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>
    
    @elseif ($message = Session::get('error'))
    
      <div class="alert alert-danger alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>

  
  @elseif (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems while adding the user. Please try again.<br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


<!--mainpage chit-chating-->
<div class="chit-chat-layer1">
    <div class="chit-chat-layer1-left">
       <div class="work-progres">
          <div class="block-heading-1">
            <h2 class="text-center">Users</h2>
          </div>
          @if(count($users) > 0)
              <div class="table-responsive">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Division</th>
                        <th>Designation</th>
                        <th>E-mail</th>
                        <th>Mobile No.</th>
                        @if(Auth::user()->is_admin)
                        <th colspan="3"> Actions </th>
                        @elseif(!Auth::user()->is_admin)
                        <th> Actions </th>
                        @endif
                    </tr>
                </thead>

                
                <tbody>
                  @foreach($users as $user)
                  <tr>
                  @if($user->location == 'NIB')
                  <td>{{ $user->user_name }}</td>
                  @elseif($user->location != 'NIB')
                  <td>{{ $user->user_name }}<span class="label label-info ml-1"> Outside </span></td>
                  @endif

                  <td>{{ $user->division }}</td>
                  <td>{{ $user->designation }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->mobile }}</td>
                  @if(Auth::user()->is_admin)
                  <td colspan="3">
                    <button class="btn btn-info"><a href="{{ route('view_profile', $user->id)}}">Details</a></button>
                    <button class="btn btn-secondary"><a href=#>Edit</a></button>
                    <button class="btn btn-danger"><a href=#>Delete</a></button>
                  </td>
                  @elseif(!Auth::user()->is_admin)
                  <td>
                    <button class="btn btn-info"><a href="{{ route('view_profile', $user->id)}}">Details</a></button>
                  </td>
                  @endif
                  </tr>
                  @endforeach
            </tbody>
            </table>
          </div>
            @elseif(count($users) == 0)
           <div class="image_container">
               <img src="{{ asset('assets/images/empty_page_cropped.png') }}" alt="Empty Result" class="center">
                <h3 class="text-center">Sorry, no users found!</h3>
            </div>
            @endif
   </div>
  </div>
  <div class="clearfix"> </div>
</div>
<!--User list end-->
</div>
<!-- inner block end -->         

@endsection

