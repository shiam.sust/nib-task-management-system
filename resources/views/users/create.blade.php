@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <h3 class="box-title text-success m-b-15">Create User <a href="{{ route('show-users') }}" class="waves-effect pull-right"><button class="btn btn-sm btn-info "><i class="fa fa-arrow-circle-left"></i> ALL USERS LIST</button></a></h3>
          <p class="text-muted m-b-30">Create New User</p>
        <hr>
    <form action="{{ route('add-users') }}" method="post">
            {{ csrf_field() }}

            <div class="form-body">
                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <p class="alert alert-danger">{{$error}}</p>
                    @endforeach
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Name <span class="text-danger m-l-5">*</span></label>
                        <input type="text" id="name" class="form-control" placeholder="Officer Name" value="{{ old('name') }}" name="name" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Designation <span class="text-danger m-l-5">*</span></label>
                            <select class="form-control" data-placeholder="Choose Designation" tabindex="1" name="designation" required="">
                                <option value="">Choose Designation</option>
                                @foreach($designations as $data)
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                <!-- Raihan has added the division_id just to be sure that division and designation integrity constraint are maintained.-->
                                <option value="{{ $data->id }}">{{ $data->division_id }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    

                </div>

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Division <span class="text-danger m-l-5">*</span></label>
                            <select class="form-control" data-placeholder="Choose Division" tabindex="1" name="division" required="">
                                <option value="">Choose Division</option>
                                @foreach($divisions as $data)
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Username </label>
                        <input type="text" id="username" class="form-control" placeholder="username" value="{{ old('username') }}" name="username">
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                        <input type="email" class="form-control" placeholder="email@example.com" value="{{ old('email') }}" name="email" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Mobile</label>
                        <input type="text" id="mobile" class="form-control" placeholder="Mobile no" value="{{ old('mobile') }}" name="mobile">
                        </div>
                    </div>
                    

                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Set Password</label>
                            <input type="password" class="form-control" placeholder="password" name="password" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Confirm Password</label>
                                <input type="password" class="form-control" placeholder="confirm password" name="password_confirmation">
                            </div>
                        </div>
                </div>
                <!--/row-->
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Role </label>
                        <input type="text" id="role" class="form-control" placeholder="role" value="{{ old('role') }}" name="role">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Is Admin <span class="text-danger m-l-5">*</span></label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-info">
                                        <input type="radio" name="is_admin" id="radio1" value=1 >
                                        <label for="radio1">yes</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-info">
                                        <input type="radio" name="is_admin" id="radio2" value=0 checked>
                                        <label for="radio2">no </label>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Register New User</button>
            </div>
        </form>
    </div>
    </div>
@endsection