@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System View Complete or Failed Task Details</title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ asset('assets/css/view_details_page.css') }}" rel="stylesheet"> 

<style>
#icon {  margin-bottom: 10px;margin-right: 10px; display: inline; font-size: 1em;}

</style>
</head>
@endsection

@section('content')
<div class="inner-block">
    <div class="chit-chat-layer1">
        <div class="chit-chat-layer1-left">
            <div class="work-progres">
                <div class="chit-chat-heading">
                    Personal Profile
                </div>
                <div class="row">
                    <div class="col-sm-5 col-md-3">
                        @if($user->image != null)
                        <img src="{{ URL::to($user->image) }}" alt="" class="img-rounded img-responsive" />

                        @elseif($user->image == null)
                        <img src="{{ URL::to('/assets/images/img-person-placeholder.jpg') }}" alt="" class="img-rounded img-responsive" />
                        @endif

                    </div>
                <div class="col-sm-7 col-md-9">
                    <h3 class="text-primary">
                        {{ $user->user_name }}
                    </h3>
                    <h5 class="text-black">
                        {{ $user->designation }}
                    </h5>
                    <h5 class="text-black">
                        Department of {{ $user->division }}
                    </h5>

                    <p class="mt-3">
                       <i class="fas fa-envelope" id="icon"></i><span class="ml-1">{{$user->email}} </span>
                        <br />
                        <i class="fas fa-phone" id="icon"></i>{{$user->mobile}}
                        <br />
                        <i class="fas fa-map-marker-alt" id="icon"></i>{{$user->location}}
                    </p>
                </div>
            </div>
            </div>
        </div>
    </div>

<div class="chit-chat-layer1">
        <div class="chit-chat-layer1-left">
            <div class="work-progres">
                <div class="chit-chat-heading">
                    Task Profile
                </div>
            <div class="market-updates">
                <div class="col-md-4 market-update-gd">
                <div class="market-update-block clr-block-3">
                    <div class="col-md-8 market-update-left">
                        <h3>{{ $ongoing }}</h3>
                        <h4>Ongoing Tasks</h4>
                    </div>
                    <div class="col-md-4 market-update-right">
                        <i class="fas fa-file-alt"> </i>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                </div>
            
            <div class="col-md-4 market-update-gd">
                <div class="market-update-block clr-block-1">
                    <div class="col-md-8 market-update-left">
                        <h3>{{ $complete }}</h3>
                        <h4>Completed Tasks</h4>
                    </div>
                    <div class="col-md-4 market-update-right">
                        <i class="fas fa-check-square"> </i>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </div>

            <div class="col-md-4 market-update-gd">
                <div class="market-update-block clr-block-2">
                 <div class="col-md-8 market-update-left">
                    <h3>{{ $fail }}</h3>
                    <h4>Failed Tasks</h4>
                  </div>
                    <div class="col-md-4 market-update-right">
                        <i class="fa fa-warning"> </i>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </div>

            <!-- -->

            <div class="clearfix"> </div>
        </div>
        <!--market updates end here-->
    </div>
    </div>
    </div>
</div>

@endsection