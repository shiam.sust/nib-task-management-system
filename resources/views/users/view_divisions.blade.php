@extends('layouts.master')

@section('head')
@parent

<title>NIB user Management System User Home </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>

<style>
  #division_name{
    font-size: 1.4em;
  }
</style>
</head>
@endsection

@section('content')   
<!--inner block start here-->
<div class="inner-block bg-white">

   @if ($message = Session::get('success'))
    
      <div class="alert alert-success alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>
    
    @elseif ($message = Session::get('error'))
    
      <div class="alert alert-danger alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>

  
  @elseif (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems while adding the user. Please try again.<br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

<div class="site-section block-feature-1-wrap" id="services-section">
    <div class="container">
      <div class="row mb-3">
        <div class="col-12">
          <div class="block-heading-1">
            <h2 class="text-center">Choose Divsion</h2>
          </div>
        </div>
      </div>
      <div class="row">
       
       @foreach($users_count as $user_count)

        <div class="col-lg-4 col-md-6 mb-5">
          <div class="block-feature-1">
            <h2 class="text-black" id="division_name">{{ $user_count->division_name }}</h2>
            <p>Total User: {{ $user_count-> users_in_division }}</p>
            <a href="{{ route('view_users_list_admin', $user_count->division_id) }}" class="btn btn-primary text-white" >View Detailed List</a>
          </div>
        </div>
        @endforeach

      </div>
    </div>
  </div>
</div>
<!-- inner block end -->         

@endsection

