@extends('layouts.form')

@section('head')
@parent

<title>NIB Task Management System Upload Profile Picture</title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')            
<!--inner block start here-->
<div class="inner-block">
  <div class="container" style="height: 500px">
    @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="card justify-content-center w-50" style="margin-left: 18em">
    <div class="card-header bg-secondary text-white">Upload Profile Picture</div>
        <div class="card-body">
            <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <h4 class="card-title text-primary">Change profile picture?</h4>
                    <form action="{{ route('store_profile_pic_2') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input type="file" class="form-control-file" name="fileToUpload" id="inputFile" aria-describedby="fileHelp" value="Change">
                        <input type="hidden" name="user_id" value="{{ $user_id }}">
                        <small id="fileHelp" class="form-text text-muted">Image size should not exceed 2MB!</small>
                    </div>
                    <button type="submit" class="btn btn-info">Upload </button>
                </form>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6">
                <img src="{{ URL::to($image) }}" width="150" height="150">
            </div>
        </div>
        </div>
        <div class="card-footer text-center bg-secondary text-white">
         <button class="btn btn-default btn-md"><a href="{{ route('self_assigned_tasks') }}">Finish</a></button>
        </div>
    </div>
</div>
</div>

<!-- inner block end -->
@endsection

