@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System Add Task </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')
    <div class="container-fluid mt-5" style="background-image: url('../assets/images/blue_4.jpg'); background-size: cover;"> <!-- mt-5 bootstrap class gives 48px of top margin -->

	<div class="login-form">

    <!-- Show message about whether file was uploaded successfully or not -->
	 @if ($message = Session::get('success'))
	  
	    <div class="alert alert-success alert-block">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	     </div>

	@endif
    
    @if ($message = Session::get('error'))
	  
	    <div class="alert alert-danger alert-block">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	     </div>

	@endif


	<div class="block-heading-1">
        <h3 class="text-center">Register</h3>
    </div>
    
    <div id="input-field">
		<form id="login-form" method="POST" class="form-signin" role="form" action="{{route('add-users')}}">

		@csrf <!-- CSRF validation-->

	    <!--User name-->
		<div class="form-group">
			<label class="control-label" for="name"> Name<span class="text-danger m-l-5">*</span></label>
			<input name="name" id="name" class="form-control" placeholder="Officer name" required>
		</div>
        
        <!--Division-->
        <div class="form-group">
        	<label class="control-label">Division<span class="text-danger m-l-5">*</span></label>
	            <select class="form-control" id="division" data-placeholder="Choose Division" tabindex="1" name="division" required="">
	                @foreach($divisions as $data)
	                <option value="{{ $data->id }}">{{ $data->name }}</option>
	                @endforeach
	            </select>
		</div>

		<!--Designation-->
        <div class="form-group">
        	<label class="control-label" for="designation"> Designation<span class="text-danger m-l-5">*</span></label>
        	<select class="form-control" id="designation" data-placeholder="Choose Designation" tabindex="1" name="designation" required="">
                
            </select>
		</div>
        
        <!--Username-->
        <div class="form-group">
        	<label class="control-label">Username </label>
                <input type="text" id="username" class="form-control" placeholder="Username" value="{{ old('username') }}" name="username">
		</div>
		
		<!--Email-->
        <div class="form-group">
        	<label class="control-label">Email<span class="text-danger m-l-5">*</span></label>
            <input type="email" class="form-control" placeholder="email@example.com" value="{{ old('email') }}" name="email" required>
		</div>

        <-- Password -->
		<div class="form-group">
            <label class="control-label">Set Password<span class="text-danger m-l-5">*</span></label>
            <input type="password" class="form-control" placeholder="Password" name="password" required>
        </div>

        <div class="form-group">
            <label class="control-label">Confirm Password<span class="text-danger m-l-5">*</span></label>
            <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation">
        </div>
        
        <!-- User Role -->
        <div class="form-group">
	        <label class="control-label">Role</label>
	        <input type="text" id="role" class="form-control" placeholder="Role" value="{{ old('role') }}" name="role">
	    </div>
        
        <!-- User admin or not -->
	    <div class="form-group">
            <label class="control-label">Is Admin <span class="text-danger m-l-5">*</span></label>
            <div class="radio-list">
                <label class="radio-inline p-4">
                    <div class="radio radio-info">
                        <input type="radio" name="is_admin" id="radio1" value=1 >
                        <label for="radio1">Yes</label>
                    </div>
                </label>
                <label class="radio-inline">
                    <div class="radio radio-info">
                        <input type="radio" name="is_admin" id="radio2" value=0 checked>
                        <label for="radio2">No </label>
                    </div>
                </label>
            </div>
        </div>

	    <button class="btn btn-block bt-login" type="submit"><h4>Add User</h4></button>
		</form>
	</div>
	</div>
   </div>

   <script type="text/javascript">
       $(document).ready(function(){
            $("#division").change(function(){
                var division_name = this.find(":selected").text();
                $.ajax({
                    type:'GET',
                    url:'/get_designations',
                    data:{divisoin_name:division_name},
                    success:function(designations){
                        $.each(designations, function(key, value) {   
                            $('#designation')
                                .append($("<option></option>")
                                    .attr("value",key)
                                    .text(value)); 
                        });
                    }
                });
            });
       });
   </script>
@endsection