@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System View Complete or Failed Task Details</title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ asset('assets/css/view_details_page.css') }}" rel="stylesheet"> 

</head>
@endsection

@section('content')
   <!--<div class="container">-->
 <div class="inner-block">
    <div class="chit-chat-layer1">
    	<div class="work-progres">
    	        <!--
                <div class="card">

                    <div class="card-body">
                        <div class="card-title mb-4">
                            <div class="d-flex justify-content-start">
                                <div class="userData ml-3">
                                    <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold">View Task Details</h2>
                                </div>
                            </div>
                        </div>
                    -->

                        <div class="chit-chat-heading">
                                  Task Details
                                  
                                  @if($task->edited and (Auth::user()->is_admin or Auth::user()->id == $task->assigned_by))
                                    <span class="label label-info ml-3">EDITED</span>
                                  @endif
                        </div> 
                        <!--
                        <div class="row">
                            <div class="col-12">
                         -->
                            	<!--
                                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Basic Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#connectedServices" role="tab" aria-controls="connectedServices" aria-selected="false">Connected Services</a>
                                    </li>
                                </ul>
                            --> 
                            <!--
                                <div class="tab-content ml-1" id="myTabContent">
                                    <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
                                        
                                -->
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Title</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                 {{ $task->title}}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Description</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->description}}		
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Weight</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->weight }}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Assigned By</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->assign_by_name}}		
                                            </div>
                                        </div>
                                        <hr />
                                        
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Assigned To</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->assign_to_name}}
                                            </div>
                                        </div>
                                        <hr />
                                        
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Assignment Date</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->assign_date }}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Status</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                @if($task->status=="Pending")
                                                <span class="label label-warning">Pending</span>

                                                @elseif($task->status=="Ongoing")
                                                <span class="label label-info">Ongoing</span>

                                                @elseif($task->status=="Completed")
                                                <span class="label label-success">Completed</span>

                                                @elseif($task->status=="Failed")
                                                <span class="label label-danger">Failed</span>

                                                @endif
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Place of Work</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->work_place }}
                                            </div>
                                        </div>
                                        <hr />

                                        
                                        <!-- This part is applicable only for completed tasks. When we make this page dynamic if the work is not completed yet then the field value of the two field below would be N/A-->
                                        @if($task->status=="Completed")
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Mark</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $c_f_task->mark}}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Comment</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $c_f_task->comment }}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Attachment</label>
                                            </div>
                                            @php
                                            {{
                                                $file_name = explode(".",$c_f_task->file_path);
                                            }}
                                            @endphp

                                            @if($file_name[1] == "doc" or $file_name[1] == "docx")
                                            <div class="col-md-8 col-4">
                                                <img src= "{{ URL::to('/images/word.png') }}" width="150" height="150">
                                            </div>

                                            @elseif($file_name[1] == "xls" or $file_name[1] == "xlsx")
                                            <div class="col-md-8 col-4">
                                                <img src= "{{ URL::to('/images/excel.png') }}" width="150" height="150">
                                            </div>

                                            @elseif($file_name[1] == "pdf")
                                            <div class="col-md-8 col-4">
                                                <img src= "{{ URL::to('/images/pdf.png') }}" width="150" height="150">
                                            </div>
                                            @else
                                            <div class="col-md-8 col-4">
                                                <img src= "{{ URL::to($c_f_task->file_path) }}" width="150" height="150">
                                            </div>
                                            @endif
                                        </div>
                                        <hr />
                                        <div class=row>
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Action</label>
                                            </div> 
                                            <div class="col-md-8 col-6 ml-4">
                                                    <button class="btn btn-success"><a style="color:#FFFFFF" href="{{ route('download_file', $c_f_task->id) }}">Download</a></button>
                                            </div>

                                        </div>
                                        <!-- 
                                            If the task is faild just show the reason of failure-->
                                          
                                        @elseif($task->status=="Failed")
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Reason</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $c_f_task->explain}}
                                            </div>
                                        </div>
                                        <hr />
                                        @endif
                                        <!--
                                        <div class="row">
                                            
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Edition</label>
                                            </div>
                                            
                                            
                                            <div class="col-md-8 col-6">
                                                <span class="label label-info">EDITED</span>
                                            </div>
                                        </div>
                                        <hr />
                                        -->

                                    
        	</div>
        </div>
    </div>
@endsection