@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System Add Task </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')
<!--style="margin-top: 110px"-->
  <div class="container-fluid mt-5"> <!-- mt-5 bootstrap class gives 48px of top margin -->
   @if ($message = Session::get('success'))
	  
	    <div class="alert alert-success alert-block mt-5">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	    </div>

	@endif

	 @if ($message = Session::get('error'))
	  
	    <div class="alert alert-danger alert-block mt-5">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	     </div>

	@endif

    <!-- Show message about whether task was added successfully or not -->
    <div class="login-form">
    <!--
	@if (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems while adding the task. Please try again.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
    -->

	<div class = "page-title">
		<h1 class="title text-center">Add New Task</h1>
	</div>
    
    <div id="input-field">
		<form id="login-form" method="POST" class="form-signin" role="form" action="{{route('add_task')}}">

		@csrf <!-- CSRF validation-->

	    <!--Title-->
		<div class="form-group">
			<label for="title"> Title<span class="text-danger ">*</span></label>
			<input name="title" id="title" class="form-control" placeholder="Title of Task">
			
			@if ($errors->has('title'))
            <div class="error text-danger">{{ $errors->first('title') }}</div>
			@endif

		</div>

		<!--Description-->
        <div class="form-group">
        	<label for="description"> Description</label>
        	<textarea name = "description" id="description" class="form-control" rows="5" placeholder="Task description..."></textarea>
		</div>

		<!--Assign to-->
		<!-- 
		Here in form user id is also shown besides user name. Because in the database only the user id will be saved not the user name since user id is the primary key of users table. Here ids are shown insdie braces and in TaskController the id is extracted using regex and saved to databse.
		-->
        <div class="form-group">
        	<label for="assign_to"> Assign To<span class="text-danger ">*</span></label>
        	<select name="assign_to" id="assign_to" class="form-control">
        		@if(Auth::user()->location == 'NIB')
        		<option value="{{Auth::id()}}">{{ Auth::user()->name }}</option>

        		@elseif(Auth::user()->location != 'NIB')
        		<option value="{{Auth::id()}}">{{ Auth::user()->name }}(Outside)</option>
                @endif

        		@foreach($officers as $officer)
        		@if($officer->location == 'NIB')
        		<option value="{{ $officer->user_id }}">{{ $officer->user_name }} </option>

        		@elseif($officer->location != 'NIB')
        		<option value="{{ $officer->user_id }}">{{ $officer->user_name }}(Outside)</option>
        		@endif
        		@endforeach
        	</select>
		</div>
        
        <!--Weight-->
        <div class="form-group">
        	<label for="weight">Weight<span class="text-danger ">*</span></label>
			<select name="weight" id="weight" class="form-control">
				<option value="10">10</option>
			    <option value="20">20</option>
			    <option value="30">30</option>
			    <option value="40">40</option>
			    <option value="50">50</option>
			    <option value="60">60</option>
			    <option value="70">70</option>
			    <option value="80">80</option>
			    <option value="90">90</option>
			    <option value="100">100</option>
			</select>
		</div>
		
		<!--Assign Date: This field is  for deadline validation-->
        <div class="form-group">
        	
			<input name="assign_date" id="assign_date" type="hidden" class="form-control" placeholder="Assign Date" value="2019-06-06"> 

		</div>

		<!--Dead Line-->
        <div class="form-group">
        	<label for="dead_line"> Deadline<span class="text-danger ">*</span></label>
			<input name="dead_line" id="dead_line" type="date" class="form-control" placeholder="Deadline"> 

			@if ($errors->has('dead_line'))
            <div class="error text-danger">{{ $errors->first('dead_line') }}</div>
			@endif
		</div>

        <!--Work Place-->
		<div class="form-group">
			<label for="location"> Place of Work<span class="text-danger ">*</span></label>
			<div class="radio-list">
                <label class="radio-inline p-4">
                    <div class="radio radio-info">
                        <input type="radio" name="location" id="radio1" value="NIB">
                        <label for="radio1">Inside NIB</label>
                    </div>
                </label>
                <label class="radio-inline">
                    <div class="radio radio-info">
                        <input type="radio" name="location" id="radio2" value="Outside NIB">
                        <label for="radio2">Outside NIB </label>
                    </div>
                </label>
            </div>
            @if ($errors->has('work_place'))
            <div class="error text-danger">{{ $errors->first('work_place') }}</div>
			@endif
		</div>

		<div class="form-group" id="work_place_div">
        	<label for="work_place">Location<span class="text-danger ">*</span></label>
			<input name="work_place" id="work_place" type="text" class="form-control" placeholder="Exact location of work"> 
		</div>

			<button class="btn btn-block bt-login" type="submit"><h4>Add Task</h4></button>
		</form>
	</div>
	</div>
   </div>

   <script type="text/javascript">
    $(document).ready(function(){
    	$("#work_place_div").hide();
        $('input[type="radio"]').click(function(){
            if($("#radio1").is(":checked")){
            	$("#work_place").val("NIB");
                $("#work_place_div").hide();
            }
            else if($("#radio2").is(":checked")){
            	$("#work_place").val("");
                $("#work_place_div").show();
            }
        });
       });
    
</script>
@endsection