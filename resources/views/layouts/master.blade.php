@section('head')
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!--bootstrap and jquery links -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-light-green.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-light-blue.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!--bootstrap and jquery links end -->

<!-- bootstrap jQuery and some other utility css -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" type="text/css">
<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/basic.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/dropzone.css') }}">

<!-- bootstrap, jQuery and some other utility css end -->

<!-- Custom Style Sheet -->
<link rel="stylesheet" href="{{ asset('assets/css/landing_page_style.css') }}">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('assets/js/jquery-2.1.1.min.js') }}"></script> 
<!--icons-css-->
<link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet" type='text/css'>
<link rel="stylesheet" href="{{ asset('assets/fonts/icomoon/style.css') }}">
<!-- fa icon 5 -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<!--static chart-->
<script src="{{ asset('assets/js/Chart.min.js') }}"></script>
<!--//charts-->
<!-- geo chart -->
    <script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <script>window.modernizr || document.write('<script src="lib/modernizr/modernizr-custom.js"><\/script>')</script>
    <!--<script src="lib/html5shiv/html5shiv.js"></script>-->
     <!-- Chartinator  -->
    <script src="{{ asset('assets/js/chartinator.js') }}" ></script>

<!--skycons-icons-->
<script src="{{ asset('assets/js/skycons.js') }}"></script>
<!--//skycons-icons-->

@show

<body>  
<div class="page-container">
@include('layouts.header')    
   <div class="left-content">
       <div class="mother-grid-inner">
		   @yield('content')

		   @include('layouts.footer')
        </div>
    </div>
    <!-- End left-content-->

    <!--sider menu-->
    <div class="sidebar-menu">
      <div class="menu">
        <ul id="menu" >

         
          <!--
          <li id="menu-comunicacao" ><a href="{{ route('add-users') }}"><span class="menu-item"><i class="fas fa-user-plus sidebar-fa-icon" aria-hidden="true"></i>Add User</span></a></li><hr/>
        -->
          
          @if(!Auth::user()->is_admin)
          <li id="menu-comunicacao" ><a href="{{ route('view_users_list') }}"><span class="menu-item"><i class="fas fa-user-tie" aria-hidden="true"></i>View Users</span></a></li><hr/>

          @elseif(Auth::user()->is_admin)
          <li id="menu-comunicacao" ><a href="{{ route('view_divisions') }}"><span class="menu-item"><i class="fas fa-user-tie" aria-hidden="true"></i>View Users</span></a></li><hr/>
          @endif

          <li id="menu-home" ><a href="{{ route('add_task') }}"><span class="menu-item"><i class="fas fa-edit sidebar-fa-icon" aria-hidden="true"></i>Add Task</span></a></li><hr/>

          <li><a href="#task-types" data-toggle="collapse" aria-expanded="false"><span class="menu-item"><i class="fas fa-file-alt sidebar-fa-icon" aria-hidden="true"></i>View Tasks</span></a>
          </li><hr/>
          
          <li class="collapse" id="task-types">
          <ul>
          <li><a href="{{ route('self_assigned_tasks') }}">Self Assigned</a></li><hr/>

          <li><a href="{{ route('tasks_assigned_to_me') }}">Assigned to Me</a></li><hr/>

          <li><a href="{{ route('tasks_assigned_by_me') }}">Assigned by Me</a></li><hr/>
          </ul>
          </li>
        </ul>
      </div>
     </div>
     <!--slide bar menu end here-->
    <div class="clearfix"> </div>
</div>
<!-- End page-container-->

<!--
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
});
</script>
-->
<!--scrolling js-->
        <script src="{{ asset('assets/js/jquery.nicescroll.js') }}"></script>
        <!--bootstrap and other utility js-->
        <script src="{{ asset('assets/js/scripts.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.js') }}"> </script>
        <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
        <script src="{{ asset('assets/js/real-time-notify.js') }}"></script>
        <script src="{{ asset('assets/js/popper.min.js') }}"></script>
        <script src="{{ asset('assets/js/dropzone.js') }}"></script>
        <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.sticky.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.animateNumber.min.js') }}"></script>
        <script src="{{ asset('assets/js/aos.js') }}"></script>
        <script src="{{ asset('assets/js/main.js') }}"></script>
<!-- mother grid end here-->
</body>
</html>                     
