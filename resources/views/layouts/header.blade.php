<!--header start here-->
                <div class="header-main container-fluid">
                    <div class="header-left">
                            <div class="logo-name">
                                <a href=#>
                                     <img id="nib_logo" src="{{ asset('assets/images/nib_logo.png') }}" alt="NIB Logo"/>
                                      <span class="text-primary" style="font-size: 1.85em">NIB</span> 
                                  </a>                              
                            </div>
                            <!--search-box-->
                            <!--
                                <div class="search-box">
                                    <form>
                                        <input type="text" placeholder="Search..." required=""> 
                                        <input type="submit" value="">                  
                                    </form>
                                </div>
                                -->
                            <div class="clearfix"> </div>
                        <!--//end-search-box-->
                         </div>
                        
                        <!-- get number of unread notifications for this user -->
                        @php
                         $user = Auth::user(); 
                         $notification_count = count($user->unreadNotifications);
                        @endphp

                         <div class="header-right">
                            <div class="profile_details_left"><!--notifications of menu start -->
                                <ul class="nofitications-dropdown">
                                    <!--general notifications start-->
                                    <li class="dropdown head-dpdn">

                                        
                                        <a id="notify-bell" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span id="notify-count" class="badge blue"></span></a>
                                        
                                        <ul class="dropdown-menu" id="notify-dropdown">
                                            
                                        </ul>
                                    </li> <!--general notifications end-->  
                                </ul>
                                <!--<div class="clearfix"> </div>-->
                            </div> <!--notification menu end -->

                            <div class="profile_details"> <!--profile details start-->      
                                <ul>
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown" aria-expanded="false">
                                        
                                            <div class="profile_img">
                                                
                                                @if(Auth::user()->image == null)
                                                <span class="prfil-img"><img src="{{ asset('assets/images/img-person-placeholder.jpg') }}" width="50" height="50" style="border-radius: 50%" alt=""> </span>

                                                @elseif(Auth::user()->image != null)
                                                <span class="prfil-img"><img src="{{ URL::to(Auth::user()->image) }}" width="50" height="50" style="border-radius: 50%" alt="" alt=""> </span>
                                                
                                                @endif
                                                <div class="user-name">
                                                    <p>{{ Auth::user()->name }}</p>
                                                </div>
                                                
                                                <span class="caret"></span>
                                                
                                                <!--
                                                <i class="fa fa-angle-down lnr"></i>
                                                <i class="fa fa-angle-up lnr"></i>-->
                                            
                                                <!--<div class="clearfix"></div>    -->
                                            </div>  
                                        </a>
                                    
                                        <ul class="dropdown-menu drp-mnu">
                                        
                                            <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li> 
                                            <li> <a href="#"><i class="fas fa-user-tie"></i> Profile</a> </li> 
                                            <li> <a href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i> Logout</a> </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div> <!--profile details end-->
                            <div class="clearfix"> </div>               
                        </div>
                     <div class="clearfix"> </div>  
                </div>
            <!--header end here-->

<!-- script-for sticky-nav -->
        
    
 <!-- When user click on bell hide the badge showing the number of unread notifications. -->
        <!--
        <script>
        $(document).ready(function() {
            $("#notify-bell").click(function(){
            $("#notify-count").css('display','none');
           });
        });
        </script>
        -->
        <script>
        $(document).ready(function() {
             var navoffeset=$(".header-main").offset().top;
             $(window).scroll(function(){
                var scrollpos=$(window).scrollTop(); 
                if(scrollpos >=navoffeset){
                    $(".header-main").addClass("fixed");
                }else{
                    $(".header-main").removeClass("fixed");
                }
             });
             
        });
        </script>
        
    

<!-- /script-for sticky-nav -->