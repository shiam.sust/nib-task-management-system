@extends('layouts.form')

@section('head')
@parent

<title>NIB Task Management System Verify Email</title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')
<div class="container" style="height: 700px">
    <div class="row justify-content-center" style="margin: 5em">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-secondary text-white">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
