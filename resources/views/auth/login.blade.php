@extends('layouts.form')

@section('head')

@parent

<title>NIB Task Management System Login </title>
<link href="{{ asset('assets/css/login_page_style.css') }}" rel="stylesheet">  
@endsection

@section('content')
<body>
     <div class="container">
    <div class="login-form">
    <div style="text-align: center">
        <img class = "nib_logo" src = "{{ asset('assets/images/nib_logo.png') }}" alt="NIB Logo" >
        <h1 class="title text-center">National Institute of Biotechnology</h1>
        <h3 class="title text-center"> Task Management System<h3>
    </div>

        <form id="login-form" method="POST" action="{{ route('login') }}" class="form-signin" role="form">
            @csrf
            <div class="form-group">
                <input name="email" id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                placeholder="Email" autofocus>
                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif 
            </div>
            <div class="form-group">
                <input name="password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="checkbox">
                <label for="remember_me">
                <input name="remember_me" id="remember_me" class="form-check-input{{ old('remember') ? 'checked' : '' }}" type="checkbox">
            {{ __('Remember Me') }}</label>
            </div>
            <button class="btn btn-block bt-login" type="submit">Sign In</button>
        </form>
        <div class="form-footer">
            <div class="row">
                <div class="col-xs-7 col-sm-7 col-md-7">
                    <i class="fa fa-lock"></i> 
                    <a href="#"> Forgot password? </a>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5">
                    <i class="fa fa-check"></i> <a href="{{ route('register') }}"> Sign Up </a>
                </div>
            </div>
        </div>
    </div>
   </div>
@endsection


{{-- @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif --}}