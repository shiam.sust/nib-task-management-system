@extends('layouts.form')

@section('head')
@parent

<title>NIB Task Management System Register </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')
    <div class="container"> <!-- mt-5 bootstrap class gives 48px of top margin -->

    <div class="login-form">

    <!-- Show message about whether user registered successfully or not -->
     @if ($message = Session::get('success'))
      
        <div class="alert alert-success alert-block">

        <button type="button" class="close" data-dismiss="alert">×</button>

            <strong>{{ $message }}</strong>

         </div>

    @endif
    
    @if ($message = Session::get('error'))
      
        <div class="alert alert-danger alert-block">

        <button type="button" class="close" data-dismiss="alert">×</button>

            <strong>{{ $message }}</strong>

         </div>

    @endif


    <div class = "page-title">
        <h1 class="title text-center">Sign Up</h1>
    </div>
    
    <div id="input-field">
        <form id="login-form" method="POST" class="form-signin" role="form" action="{{route('register')}}">

        @csrf <!-- CSRF validation-->

        <!--User name-->
        <div class="form-group">
            <label class="control-label" for="name">Name<span class="text-danger m-l-5">*</span></label>
            <input name="name" id="name" class="form-control" placeholder="Officer name" >

            @if ($errors->has('name'))
            <div class="error text-danger">{{ $errors->first('name') }}</div>
            @endif

        </div>
        
        <!--Division-->
        <div class="form-group">
            <label class="control-label">Division<span class="text-danger m-l-5">*</span></label>
                <select class="form-control" id="division" data-placeholder="Choose Division" tabindex="1" name="division" >
                    <option>Choose Division</option> 
                    @foreach($divisions as $data)
                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                    @endforeach
                </select>

                @if ($errors->has('division'))
                <div class="error text-danger">{{ $errors->first('division') }}</div>
                @endif
        </div>

        <!--Designation-->
        <div class="form-group">
            <label class="control-label" for="designation">Designation<span class="text-danger m-l-5">*</span></label>
            <select class="form-control" id="designation" data-placeholder="Choose Designation" tabindex="1" name="designation">
            </select>

            @if ($errors->has('designation'))
            <div class="error text-danger">{{ $errors->first('designation') }}</div>
            @endif
        </div>
        
        <!--Username-->
        <div class="form-group">
            <label class="control-label">Username </label>
            <input type="text" id="username" class="form-control" placeholder="Username" value="{{ old('username') }}" name="username">
        </div>
        
        <!--Email-->
        <div class="form-group">
            <label class="control-label">Email<span class="text-danger m-l-5">*</span></label>
            <input type="email" class="form-control" placeholder="email@example.com" value="{{ old('email') }}" name="email" >

            @if ($errors->has('email'))
            <div class="error text-danger">{{ $errors->first('email') }}</div>
            @endif
        </div>

            <div class="form-group">
                <label class="control-label">Mobile</label>
            <input type="text" id="mobile" class="form-control" placeholder="Mobile no" value="{{ old('mobile') }}" name="mobile">
            </div>

        <div class="form-group">
            <label class="control-label">Set Password<span class="text-danger m-l-5">*</span></label>
            <input type="password" class="form-control" placeholder="Password" name="password">

            @if ($errors->has('password'))
            <div class="error text-danger">{{ $errors->first('password') }}</div>
            @endif
        </div>

        <div class="form-group">
            <label class="control-label">Confirm Password<span class="text-danger m-l-5">*</span></label>
            <input type="password" class="form-control" placeholder="Confirm password" name="password_confirmation">
        </div>

        <div class="form-group">
            <label class="control-label">Role</label>
            <input type="text" id="role" class="form-control" placeholder="Role" value="{{ old('role') }}" name="role">
        </div>

        <div class="form-group">
            <label class="control-label">Is Admin <span class="text-danger m-l-5">*</span></label>
            <div class="radio-list">
                <label class="radio-inline p-4">
                    <div class="radio radio-info">
                        <input type="radio" name="is_admin" id="radio1" value=1 >
                        <label for="radio1">Yes</label>
                    </div>
                </label>
                <label class="radio-inline">
                    <div class="radio radio-info">
                        <input type="radio" name="is_admin" id="radio2" value=0 checked>
                        <label for="radio2">No </label>
                    </div>
                </label>
            </div>
        </div>

        <button class="btn btn-block bt-login" type="submit"><h4>Next</h4></button>
        </form>
    </div>
    </div>
   </div>
    

   <!-- Ajax method to show all the designatios of selected divisoin -->
   <script type="text/javascript">
       $(document).ready(function(){
            $("#division").change(function(){
                var division_name = $(this).find(":selected").text();

                $.ajax({
                    type:'GET',
                    url:'/get_designations',
                    data:{division_name:division_name},
                    success:function(designations){
                       var len = designations.length;
                        $("#designation").find("option") // clear options first
                                         .remove()
                                         .end();
                       var i, designation;
                        for(i = 0; i < len; ++i){
                            designation = designations[i];
                                $("#designation").append($("<option></option>")
                                    .attr('value',designation.id)
                                    .text(designation.name)); 
                        }
                    }
                });
            });
        });
   </script>
@endsection