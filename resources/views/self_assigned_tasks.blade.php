@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System User Home </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>

<style>

  /**
  * fix the position and the size of image that is displayed when there is no tasks
  * available to display
  */
  .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 25%;
}

</style>

</head>
@endsection

@section('content')            
<!--inner block start here-->
<div class="inner-block">

  <!-- Show message about whether file was uploaded successfully or not -->
   @if ($message = Session::get('success'))
    
      <div class="alert alert-success alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>
    
    @elseif ($message = Session::get('error'))
    
      <div class="alert alert-danger alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>

  
  @elseif (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems while adding the task. Please try again.<br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif


<div class="chit-chat-layer1 mt-n1">
    <div class="chit-chat-layer1-left">
      <div class="work-progres">
        <div class="block-heading-1">
          <h3 class="text-center" style="color:#00bcd4;">Self Assigned Tasks</h3>
        </div>
      </div>
    </div>
  </div>
  
<!--market updates updates-->
     <div class="market-updates">
      <div class="col-md-3 market-update-gd">
        <div class="market-update-block clr-block-4">
            <div class="col-md-7 market-update-left">
                <h3>{{ $pending }}</h3>
                <h4>Pending Tasks</h4>
            </div>
            <div class="col-md-5 market-update-right">
                <i class="fas fa-hourglass-start"></i>
            </div>
          <div class="clearfix"> </div>
        </div>
      </div>

      <div class="col-md-3 market-update-gd">
        <div class="market-update-block clr-block-3">
            <div class="col-md-7 market-update-left">
                <h3>{{ $ongoing }}</h3>
                <h4>Ongoing Tasks</h4>
            </div>
            <div class="col-md-5 market-update-right">
                <i class="fas fa-people-carry"> </i>
            </div>
          <div class="clearfix"> </div>
        </div>
      </div>

            <div class="col-md-3 market-update-gd">
                <div class="market-update-block clr-block-1">
                    <div class="col-md-7 market-update-left">
                        <h3>{{ $completed }}</h3>
                        <h4>Completed Tasks</h4>
                    </div>
                    <div class="col-md-5 market-update-right">
                        <i class="fas fa-check-square"> </i>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </div>
            <div class="col-md-3 market-update-gd">
                <div class="market-update-block clr-block-2">
                 <div class="col-md-7 market-update-left">
                    <h3>{{ $failed }}</h3>
                    <h4>Failed <br/> Tasks</h4>
                  </div>
                    <div class="col-md-5 market-update-right">
                        <i class="fa fa-warning"> </i>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </div>
           <div class="clearfix"> </div>
        </div>
<!--market updates end here-->

<!--Self Assigned Ongoing Task Start -->
<div class="chit-chat-layer1">
  <div class="chit-chat-layer1-left">
      <div class="work-progres">
        
        <!-- table heading start-->
        <div class="block-heading-1">
          <h3 class="text-center">Ongoing</h3>
        </div>
        <!-- table heading end-->

          <!-- table start-->
          @if($ongoing > 0)
          <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Task Name</th>
                    <th>Deadline</th>
                    <th>Status</th>
                    <th colspan="5"> Actions </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($tasks as $task)
                  @if($task->status == "Ongoing")
                  <tr>
                  <td>{{ $task->title }}</td>
                  <td>{{ $task->dead_line}}</td>
                  <td><span class="label label-info">{{ $task->status }}</span></td>
                  <td colspan="5">
                    <button class="btn btn-info"><a href="{{ route('view_task_details', $task->id) }}">Details</a></button>
                    <button class="btn btn-secondary"><a href="{{ route('edit_task', $task->id) }}">Edit</a></button>
                    <button class="btn btn-warning"><a href="{{ route('request_self_deadline_extension', $task->id) }}" >Request</a></button>
                    <button class="btn btn-success"><a href="{{ route('upload_file_self', $task->id) }}">Submit</a></button>
                    <button class="btn btn-danger"><a href="{{ url('delete_task/'. $task->id.'/self') }}">Delete</a></button>
                  </td>
                  </tr>
                  @endif
                  @endforeach
            </tbody>
          </table>
        </div>
        <!-- table end -->
        
        <!-- if table is empty then show image and empty message start -->
        @elseif($ongoing == 0)
        <div class="image_container">
          <img src="{{ asset('assets/images/empty_page_cropped.png') }}" alt="Empty Result" class="center">
          <h3 class="text-center">No ongoing tasks available!</h3>
        </div>
        @endif
        <!-- if table is empty then show image and empty message end -->
      </div>
    </div>
    <div class="clearfix"> </div>
  </div>
<!--Self Assigned Ongoing Task End-->

<!--Self Assigned Completed Task Start-->
    <div class="chit-chat-layer1">
      <div class="chit-chat-layer1-left">
        <div class="work-progres">

        <!-- table heading start -->
        <div class="block-heading-1">
          <h3 class="text-center">Completed</h3>
        </div>
        <!-- table heading end -->
          
          <!-- table start -->
          @if($completed > 0)
          <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Task Name</th>
                    <th>Deadline</th>
                    <th>Status</th>
                    <th colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
              @foreach($tasks as $task)
              @if($task->status == "Completed")
              <tr>
              <td>{{ $task->title }}</td>
              <td>{{ $task->dead_line}}</td>
              
              <td><span class="label label-success">{{ $task->status }}</span></td>
              <td colspan="3">
                <button class="btn btn-info"><a href="{{ route('view_c_f_task_details', $task->id) }}">Details</a></button>
                <button class="btn btn-secondary"><a href="{{ url('remark_task/'. $task->id.'/self') }}">Remark</a></button>
                <button class="btn btn-secondary"><a href="{{ url('change_file/'. $task->task_id.'/self') }}">Change File</a></button>
              </td>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- table end -->
        
        <!-- if table is empty then show image and empty message start -->
        @elseif($completed == 0)
        <div class="image_container">
          <img src="{{ asset('assets/images/empty_page_cropped.png') }}" alt="Empty Result" class="center">
          <h3 class="text-center">No completed tasks available!</h3>
        </div>
        @endif
        <!-- if table is empty then show image and empty message end -->
      </div>
    </div>
    <div class="clearfix"> </div>
  </div>
<!--Self Assigned Completed Task End-->

<!--Self Assigned Failed Task Start-->
    <div class="chit-chat-layer1">
      <div class="chit-chat-layer1-left">
        <div class="work-progres">

        <!-- table heading start -->
        <div class="block-heading-1">
          <h3 class="text-center">Failed</h3>
        </div>
        <!-- table heading end -->
          
          <!-- table start -->
          @if($failed > 0)
          <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Task Name</th>
                    <th>Deadline</th>
                    <th>Status</th>
                    <th colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
              @foreach($tasks as $task)
              @if($task->status == "Failed")
              <tr>
              <td>{{ $task->title }}</td>
              <td>{{ $task->dead_line}}</td>
              
              <td><span class="label label-success">{{ $task->status }}</span></td>
              <td colspan="2">
                <button class="btn btn-info"><a href="{{ route('view_c_f_task_details', $task->id) }}">Details</a></button>
                <button class="btn btn-secondary"><a href="{{ url('remark_task/'. $task->id.'/self') }}">Explain</a></button>
              </td>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- table end -->
        
        <!-- if table is empty then show image and empty message start -->
        @elseif($failed == 0)
        <div class="image_container">
          <img src="{{ asset('assets/images/empty_page_cropped.png') }}" alt="Empty Result" class="center">
          <h3 class="text-center">No failed tasks available!</h3>
        </div>
        @endif
        <!-- if table is empty then show image and empty message end -->
      </div>
    </div>
    <div class="clearfix"> </div>
  </div>
<!--Self Assigned Failed Task End-->
</div>
<!-- inner block end -->
@endsection

