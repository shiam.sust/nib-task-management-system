@extends('layouts.form')

@section('head')

@parent

<title>NIB Task Management System Login </title>
<link href="{{ asset('assets/css/login_page_style.css') }}" rel="stylesheet">  
</head>
@endsection

@section('content')
<div class="container">
     	@if($errors->any())
  <h4>{{$errors->first()}}</h4>
@endif
	<div class="login-form">
	<div class="text-center">
		<img class = "nib_logo" src = "{{ asset('assets/images/nib_logo.png') }}" alt="NIB Logo" >
		<h1 class="title text-center">National Institute of Biotechnology</h1>
		<h3 class="title text-center"> Task Management System<h3>
	</div>

		<form id="login-form" method="post" class="form-signin" role="form">
		<div class="form-group">
			<input name="employee_id" id="employee_id" type="text" class="form-control" placeholder="Employee ID" autofocus> 
		</div>
		<div class="form-group">
			<input name="password" id="password" type="password" class="form-control disable" placeholder="Password">
		</div>
		<div class="form-group form-check">
			<label class="form-check-label">
			<input name="remember_me" id="remember_me" class="form-check-input" type="checkbox">
		Remember Me</label>
		</div>
			<button class="btn btn-block bt-login" type="submit">Sign In</button>
		</form>
		<div class="form-footer">
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7">
					<i class="fa fa-lock"></i> 
					<a href="#"> Forgot password? </a>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5">
					<i class="fa fa-check"></i> <a href="#"> Sign Up </a>
				</div>
			</div>
		</div>
	</div>
   </div>
@endsection
