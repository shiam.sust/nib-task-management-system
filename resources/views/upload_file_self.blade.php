@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System User Home </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')            
<!--inner block start here-->
<div class="inner-block">
  <div class="container" style="height: 500px">
    @if ($errors->any())
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row justify-content-center">
            <div class="card">
                <div class="card-header">Upload File</div>
  
                <div class="card-body">
                        <form action="{{ route('store_image_self') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="file" class="form-control-file" name="fileToUpload" id="inputFile" aria-describedby="fileHelp">
                                <input type="hidden" name="task_id" value="{{ $task_id }}">
                                <small id="fileHelp" class="form-text text-muted">Please upload word document, excel sheet, pdf or image only!</small>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                </div>
            </div>
        </div>
      </div>
    </div>
<!-- inner block end -->
@endsection

