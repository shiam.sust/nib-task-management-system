@extends('layouts.master')

@section('head')
@parent
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
@endsection

@section('content')            
<!--inner block start here-->
<div class="inner-block">
<!--market updates updates-->
     <div class="market-updates">
            <div class="col-md-4 market-update-gd">
                <div class="market-update-block clr-block-1">
                    <div class="col-md-8 market-update-left">
                        <h3>83</h3>
                        <h4>Registered User</h4>
                        <p>Other hand, we denounce</p>
                    </div>
                    <div class="col-md-4 market-update-right">
                        <i class="fa fa-file-text-o"> </i>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </div>
            <div class="col-md-4 market-update-gd">
                <div class="market-update-block clr-block-2">
                 <div class="col-md-8 market-update-left">
                    <h3>135</h3>
                    <h4>Daily Visitors</h4>
                    <p>Other hand, we denounce</p>
                  </div>
                    <div class="col-md-4 market-update-right">
                        <i class="fa fa-eye"> </i>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </div>
            <div class="col-md-4 market-update-gd">
                <div class="market-update-block clr-block-3">
                    <div class="col-md-8 market-update-left">
                        <h3>23</h3>
                        <h4>New Messages</h4>
                        <p>Other hand, we denounce</p>
                    </div>
                    <div class="col-md-4 market-update-right">
                        <i class="fa fa-envelope-o"> </i>
                    </div>
                  <div class="clearfix"> </div>
                </div>
            </div>
           <div class="clearfix"> </div>
        </div>
<!--market updates end here-->



<!--mainpage chit-chating-->
<div class="chit-chat-layer1">
    <div class="col-md-7 chit-chat-layer1-left">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Self Assigned Ongoing Tasks
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      <th>Status</th>
                                      <th>Dead Line</th>
                                      <th>Show Details</th>
                                      <th>Edit Task</th>
                                      <th>Submit</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Face book</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-primary">Edit Task</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Twitter</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-primary">Edit Task</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Google</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-primary">Edit Task</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>LinkedIn</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-primary">Edit Task</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Tumblr</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-primary">Edit Task</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Tesla</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-primary">Edit Task</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
             </div>
      </div>

      <div class="col-md-5 chit-chat-layer1-rit">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Self Assigned Completed Tasks
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      <th>Status</th>
                                      <th>Dead Line</th>
                                      <th>Show Details</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Face book</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Twitter</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Google</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>LinkedIn</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Tumblr</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Tesla</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
             </div>
      </div>
      <div class="clearfix"> </div>
</div>


<!--mainpage chit-chating-->
<div class="chit-chat-layer1">
    <div class="col-md-7 chit-chat-layer1-left">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Authority Assigned Ongoing Tasks
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      <th>Status</th>
                                      <th>Dead Line</th>
                                      <th>Show Details</th>
                                      <th>Extend Deadline</th>
                                      <th>Submit Task</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Face book</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Twitter</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Google</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>LinkedIn</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Tumblr</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Tesla</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
             </div>
      </div>

      <div class="col-md-5 chit-chat-layer1-rit">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Authority Assigned Completed Tasks
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      <th>Status</th>
                                      <th>Dead Line</th>
                                      <th>Show Details</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Face book</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Twitter</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Google</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>LinkedIn</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Tumblr</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Tesla</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
             </div>
      </div>
      <div class="clearfix"> </div>
</div>



<!--mainpage chit-chating-->
<div class="chit-chat-layer1">
    <div class="col-md-7 chit-chat-layer1-left">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Authority Assigned Ongoing Tasks
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      <th>Assigned To</th>
                                      <th>Status</th>
                                      <th>Dead Line</th>
                                      <th>Show Details</th>
                                      <th>Edit Task</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Face book</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Twitter</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Google</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>LinkedIn</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Tumblr</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                              <tr>
                                  <td>Tesla</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                                  <td><button class="btn btn-warning">Request</button></td>
                                  <td><button class="btn btn-success">Submit</button></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
             </div>
      </div>

      <div class="col-md-5 chit-chat-layer1-rit">
               <div class="work-progres">
                            <div class="chit-chat-heading">
                                  Authority Assigned Completed Tasks
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th>Task Name</th>
                                      <th>Status</th>
                                      <th>Dead Line</th>
                                      <th>Show Details</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Face book</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Twitter</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Google</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>LinkedIn</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Tumblr</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                              <tr>
                                  <td>Tesla</td>
                                  <td><span class="label label-danger">in progress</span></td>
                                  <td>20 March 2019</td>
                                  <td><button class="btn btn-info">Details</button></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
             </div>
        </div>
      <div class="clearfix"> </div>
    </div>
</div>
<!-- inner block end here -->
@endsection

