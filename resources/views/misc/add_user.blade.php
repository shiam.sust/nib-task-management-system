@extends('layouts.form')

@section('head')
@parent

<title>NIB Task Management System Add New User </title>
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
@endsection

@section('content')
<body>
    <div class="container" style="background-image: url('../assets/images/white_14.jpg'); background-size: cover;">
	<div class="login-form">
	<div class = "page-title">
		<h1 class="title text-center">Add New User</h1>
	</div>
    
    <div id="input-field">
		<form id="login-form" method="post" class="form-signin" role="form">
	    <!--name-->
		<div class="form-group">
			<input name="name" id="name" type="text" class="form-control" placeholder="Full Name"> 
		</div>
		<!--Employee ID-->
        <div class="form-group">
			<input name="employee_id" id="employee_id" type="text" class="form-control" placeholder="Employee ID"> 
		</div>
        <!--Designation-->
		<div class="form-group">
			<select name="designation" class="form-control">
				<option value="designation">Designation</option>
			    <option value="director_general">Director General</option>
			    <option value="head_of_department ">Head of Department</option>
			    <option value="senior_scientific_officer">Senior Scientific Officer</option>
			    <option value="scientific_officer">Scientific Officer</option>
			    <option value="techincal_staff">Technical Staff</option>
			    <option value="lab_assistant">Lab Assistant</option>
			 </select>
		</div>
        <!--Division-->
		<div class="form-group">
			<select name="division" class="form-control">
				<option value="division">Division</option>
			    <option value="animal_biotechnology">Animal Biotechnology</option>
			    <option value="environmental_biotechnology">Environmental Biotechnology</option>
			    <option value="fisheries_biotechnology">Fisheries Biotechnology</option>
			    <option value="microbial_biotechnology">Microbial Biotechnology</option>
			    <option value="molecular_biotechnology">Molecular Biotechnology</option>
			    <option value="plant_biotechnology">Plant Biotechnology</option>
			</select>
		</div>
		<!--email-->
		<div class="form-group">
			<input name="e_mail" id="e_mail" type="email" class="form-control" placeholder="E-mail" > 
		</div>
		<!--phone no-->
		<div class="form-group">
			<input name="phone_no" id="phone_no" type="text" class="form-control" placeholder="Phone No"> 
		</div>
			<button class="btn btn-block bt-login" type="submit">Add User</button>
		</form>
	</div>
	</div>
   </div>
@endsection