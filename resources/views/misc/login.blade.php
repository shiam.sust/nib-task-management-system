<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Responsive Login Page</title>
		
		<!-- Bootstrap -->
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		 <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link href="css/login_page_style.css" rel="stylesheet">  
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
<body>
     <div class="container">
	<div class="login-form">
	<div style="text-align: center">
		<img class = "nib_logo" src = "images/nib_logo.png" alt="NIB Logo" >
		<h1 class="title text-center">National Institute of Biotechnology</h1>
		<h3 class="title text-center"> Task Management System<h3>
	</div>

		<form id="login-form" method="post" class="form-signin" role="form">
		<div class="form-group">
			<input name="email" id="email" type="email" class="form-control"placeholder="Email address" autofocus> 
		</div>
		<div class="form-group">
			<input name="password" id="password" type="password" class="form-control disable" placeholder="Password">
		</div>
		<div class="form-group form-check">
			<label class="form-check-label">
			<input name="remember_me" id="remember_me" class="form-check-input" type="checkbox">
		Remember Me</label>
		</div>
			<button class="btn btn-block bt-login" type="submit">Sign In</button>
		</form>
		<div class="form-footer">
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7">
					<i class="fa fa-lock"></i> 
					<a href="#"> Forgot password? </a>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5">
					<i class="fa fa-check"></i> <a href="#"> Sign Up </a>
				</div>
			</div>
		</div>
	</div>
   </div>

	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
