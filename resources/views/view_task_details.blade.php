@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System Add Task </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ asset('assets/css/view_details_page.css') }}" rel="stylesheet"> 

</head>
@endsection

@section('content')
   <!--<div class="container">-->
 <div class="inner-block">
    <div class="chit-chat-layer1">
    	<div class="work-progres">
            
            <div class="chit-chat-heading">
                  Task Details
                  
                  @if($task->edited and (Auth::user()->is_admin or Auth::user()->id == $task->assigned_by))
                    <span class="label label-info ml-3">EDITED</span>
                  @endif
            </div> 
            
                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Title</label>
                        </div>
                        <div class="col-md-8 col-6">
                             {{ $task->title}}
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Description</label>
                        </div>
                        <div class="col-md-8 col-6">
                            {{ $task->description}}		
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Assigned By</label>
                        </div>
                        <div class="col-md-8 col-6">
                            {{ $task->assign_by_name}}      
                        </div>
                    </div>
                    <hr />
                    
                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Assigned To</label>
                        </div>
                        <div class="col-md-8 col-6">
                            {{ $task->assign_to_name}}
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Weight</label>
                        </div>
                        <div class="col-md-8 col-6">
                            {{ $task->weight }}
                        </div>
                    </div>
                    <hr />
                    
                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Assignment Date</label>
                        </div>
                        <div class="col-md-8 col-6">
                            {{ $task->assign_date }}
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Deadline</label>
                        </div>
                        <div class="col-md-8 col-6">
                            {{ $task->dead_line }}
                        </div>
                    </div>
                    <hr />


                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Status</label>
                        </div>
                        <div class="col-md-8 col-6">
                            @if($task->status=="Pending")
                            <span class="label label-warning">Pending</span>

                            @elseif($task->status=="Ongoing")
                            <span class="label label-info">Ongoing</span>

                            @elseif($task->status=="Completed")
                            <span class="label label-success">Completed</span>

                            @elseif($task->status=="Failed")
                            <span class="label label-danger">Failed</span>

                            @endif
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-sm-3 col-md-2 col-5">
                            <label style="font-weight:bold;">Place of Work</label>
                        </div>
                        <div class="col-md-8 col-6">
                            {{ $task->work_place }}
                        </div>
                    </div>
                    <hr />
        	</div>
        </div>
    </div>
@endsection