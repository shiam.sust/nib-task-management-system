@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System Add Task </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ asset('assets/css/view_details_page.css') }}" rel="stylesheet"> 

</head>
@endsection

@section('content')
   <!--<div class="container">-->
 <div class="inner-block">

 	<!-- Show message about whether the task was successfully or not -->
	 @if ($message = Session::get('success'))
	  
	    <div class="alert alert-success alert-block">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	     </div>
    
    @elseif ($message = Session::get('error'))
	  
	    <div class="alert alert-danger alert-block">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	     </div>

	@elseif (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems while adding the task. Please try again.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

    <div class="chit-chat-layer1">
    	<div class="work-progres">
    	        <!--
                <div class="card">

                    <div class="card-body">
                        <div class="card-title mb-4">
                            <div class="d-flex justify-content-start">
                                <div class="userData ml-3">
                                    <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold">View Task Details</h2>
                                </div>
                            </div>
                        </div>
                    -->

                        <div class="chit-chat-heading">
                                  Deadline Extension Request
                        </div> 
                        <!--
                        <div class="row">
                            <div class="col-12">
                         -->
                            	<!--
                                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Basic Info</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#connectedServices" role="tab" aria-controls="connectedServices" aria-selected="false">Connected Services</a>
                                    </li>
                                </ul>
                            --> 
                            <!--
                                <div class="tab-content ml-1" id="myTabContent">
                                    <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
                                        
                                -->
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Title</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                 {{ $task->title}}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Description</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->description}}		
                                            </div>
                                        </div>
                                        <hr />

                                        <!--
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Weight</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->weight }}
                                            </div>
                                        </div>
                                        <hr />
                                    -->                                        
                                        
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Assigned To</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $user->name}}
                                            </div>
                                        </div>
                                        <hr />
                                        
                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Assignment Date</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->assign_date }}
                                            </div>
                                        </div>
                                        <hr />


                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Place of Work</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->work_place }}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Message</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->message }}
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Expected Deadline</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                                {{ $task->expect_deadline }}
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                </div>
<div class="chit-chat-layer1">
    <div class="work-progres">
        <div class="login-form">
			@if ($message = Session::get('error'))
			    
			    <div class="alert alert-danger alert-block">

			      <button type="button" class="close" data-dismiss="alert">×</button>

			          <strong>{{ $message }}</strong>

			       </div>

			@endif

				<div class = "page-title">
					<h1 class="title text-center">Extend Deadline</h1>
				</div>
			    
			    <div id="input-field">
					<form id="login-form" method="POST" class="form-signin" role="form" action="{{ route('extend_deadline', $task->id) }}">
			        
					@csrf <!-- CSRF validation -->

				    <!--Deadline-->
					<div class="form-group">
						<label for="deadline"> Deadline:</label>
						<input name="deadline" id="deadline" class="form-control" type="date"  value="{{ $task->dead_line }}" required>
					</div>
					<button class="btn btn-block bt-login" type="submit"><h4>Extend</h4></button>
				</form>
			</div>
		</div>

                                    
        	</div>
        </div>
    </div>
@endsection