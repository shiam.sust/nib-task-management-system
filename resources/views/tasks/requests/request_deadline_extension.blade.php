@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System Add Task </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')
    <div class="container-fluid mt-5"> <!-- mt-5 bootstrap class gives 48px of top margin -->

	<div class="login-form">
		
    <!-- Show message about whether file was uploaded successfully or not -->
	 @if ($message = Session::get('success'))
	  
	    <div class="alert alert-success alert-block">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	     </div>
    
    @elseif ($message = Session::get('error'))
	  
	    <div class="alert alert-danger alert-block">

	    <button type="button" class="close" data-dismiss="alert">×</button>

	        <strong>{{ $message }}</strong>

	     </div>

	
	@elseif (count($errors) > 0)
	    <div class="alert alert-danger">
	        <strong>Whoops!</strong> There were some problems while adding the task. Please try again.<br><br>
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<div class = "page-title">
		<h1 class="title text-center">Request Deadline Extension</h1>
	</div>
    
    <div id="input-field">
		<form id="login-form" method="POST" class="form-signin" role="form" action="{{route('request_deadline_extension', $task_id)}}">

		@csrf <!-- CSRF validation-->

	    <!--Title-->
		<div class="form-group">
			<label for="expect_deadline">Expected Deadline:</label>
			<input name="expect_deadline" id="expect_deadline" class="form-control" type="date" required>
		</div>

		<!--Message-->
        <div class="form-group">
        	<label for="message">Message:</label>
        	<textarea name="message" id="message" class="form-control" rows="5" placeholder="Write your message here..."></textarea>
		</div>

			<button class="btn btn-block bt-login" type="submit"><h4>Send Request</h4></button>
		</form>
	</div>
	</div>
   </div>
@endsection