@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System Edit Task </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/reg_page_style.css') }}" rel="stylesheet"> 
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>
</head>
@endsection

@section('content')
<div class="container mt-5 mb-5">

    <div class="login-form">
   <!-- Show message about whether file was uploaded successfully or not -->
   @if ($message = Session::get('success'))
    
      <div class="alert alert-success alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>
    
    @elseif ($message = Session::get('error'))
    
      <div class="alert alert-danger alert-block">

      <button type="button" class="close" data-dismiss="alert">×</button>

          <strong>{{ $message }}</strong>

       </div>

  
  @elseif (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems while adding the task. Please try again.<br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  
	<div class = "page-title">
		@if($task->status == "Completed")
		<h1 class="title text-center">Remark Task</h1>

		@elseif($task->status == "Failed")
		<h1 class="title text-center">Explain Reason of Failure</h1>
		@endif

	</div>
    
    <div id="input-field">
		<form id="login-form" method="POST" class="form-signin" role="form" action="{{ url('store_remark/'.$task->id.'/'.$page_name) }}">
        
		@csrf <!-- CSRF validation -->

		@if($task->status == "Completed")
		<!--Mark-->
		<div class="form-group">
			<label for="mark"> Mark:</label>
			<input class="form-control" name="mark" id="mark" type="number" value="{{ $c_f_task->mark }}">
		</div>

		<!--Comment-->
		<div class="form-group">
			<label for="comment"> Comment:</label>
			<textarea class="form-control" name="comment" id="comment" type="text" value="{{ $c_f_task->comment }}">{{ $c_f_task->comment }}</textarea> 
		</div>

		@elseif($task->status == "Failed")
		<div class="form-group">
			<label for="explain"> Reason of Failure:</label>
			<textarea class="form-control" name="explain" id="explain" type="text" value="{{ $c_f_task->explain }}">{{ $c_f_task->explain }}</textarea> 
		</div>
		@endif

			<button class="btn btn-block bt-login" type="submit"><h4>Update</h4></button>
		</form>
	</div>
	</div>
   </div>
@endsection