@extends('layouts.master')

@section('head')
@parent

<title>NIB Task Management System View Completed Tasks </title>
<!-- Custom Theme files -->
<link href="{{ asset('assets/css/user_home_page_style.css') }}" rel="stylesheet" type="text/css" media="all"/>

<style>

  /**
  * fix the position and the size of image that is displayed when there is no tasks
  * available to display
  */
  .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 25%;
}

/**
* we added some top margin to footer so the footer does not get into main page
* this property is only applicable for this page
*
*/
.copyrights{
  margin-top:100px;
}

/**
* we changed the bg color of left-content class only for this page just to solve the problem
* with page footer
*/
.left-content{
  background-color: #fafafa;
}

</style>

</head>
@endsection

@section('content')            
<!--inner block start here-->
<div class="inner-block">

<!-- Completed Tasks Assigned by Me Start-->
  <div class="chit-chat-layer1">
    <div class="chit-chat-layer1-left">
      <div class="work-progres">

        <!-- table heading start-->
        <div class="block-heading-1">
          <h3 class="text-center">New Completed Tasks</h3>
        </div>
        <!-- table heading end-->

        <!-- table start-->
        @if($completed_tasks_count > 0)
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Task Name</th>
                <th>Assigned By</th>
                <th>Deadline</th>
                <th>Status</th>
                <th>
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($completed_tasks as $completed_task)
              <tr>
              <td>{{ $completed_task->title }}</td>
              <td>{{ $completed_task->assign_to_name }}</td>
              <td>{{ $completed_task->dead_line}}</td>
              <td><span class="label label-success">{{ $completed_task->status }}</span></td>
              
              <td>
                <button class="btn btn-info"><a href="{{ route('view_c_f_task_details', $completed_task->task_id) }}">Details</a></button>
              </td>
            </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @elseif($completed_tasks_count == 0)
        <div class="image_container">
          <img src="{{ asset('assets/images/empty_page_cropped.png') }}" alt="Empty Result" class="center">
          <h3 class="text-center">No new completed tasks available!</h3>
        </div>
        @endif
        <!-- table end -->
      </div>
    </div>
  </div>
  <!-- Completed Tasks Assigned to Me End -->

</div>
<!-- inner block end -->
@endsection
